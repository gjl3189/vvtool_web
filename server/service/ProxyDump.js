/**
 * 把可用的代理保存到文件
 * @type {ProxyDump}
 */

var App = require("../common/App.js");
var Util = require("../util/util.js");
var UsableProxyPool = require("../service/UsableProxyPool.js");
var path = require("path");
var fs = require("fs");
module.exports = new ProxyDump();

function ProxyDump(){
    //单例模式
    if(typeof arguments.callee.instance === "object"){
        return arguments.callee.instance;
    }
    arguments.callee.instance = this;
}

ProxyDump.prototype.save = function(){
    var savePath = App.config.proxy_tmp_save;
    var saveDir = path.dirname(savePath);
    Util.mkdir(saveDir);
    var jsonList = UsableProxyPool.getList();
    var proxyList = jsonList.list
    var list = [];
    if(proxyList==undefined){
        return false;
    }
    for(var i in proxyList){
        if(proxyList[i] instanceof Array){
            var len = proxyList[i].length;
            for(var j=0;j<len;j++){
                list.push(proxyList[i][j]);
            }
        }
    }

    try{
        fs.writeFileSync(savePath,JSON.stringify(list));
    }catch (e){
        console.log(e);
    }

}
