var App = require("../common/App.js"),
    request = require("request")

module.exports = new Notice();

function Notice(){
	//单例模式
    if(typeof arguments.callee.instance === "object"){
        return arguments.callee.instance;
    }

    arguments.callee.instance = this;	
}

/**
 * 通知其他验证代理的服务器，服务端刚刚启动，要求客户端重启
 */
Notice.prototype.clientRestart = function(){
    var serverList = App.config.ServerList;
    for(var i=0;i<serverList.length;i++){
        var port = serverList[i].appport;
        var serverIp = serverList[i].host;
        try{
            var url = "http://"+serverIp+":"+port+"/system/exit";
            console.log(url)
            request.get(url).on("error",function(e){
                console.log(e);
            });
        }catch (e){
            console.log(e)
        }
    }
}

Notice.prototype.delKeepalive = function(serverIp,proxy){
    var serverList = App.config.ServerList;
    var port = "";
	for(var i=0;i<serverList.length;i++){
        if(serverIp==serverList[i].host){
            port = serverList[i].appport;
        }
    }
    if(port!=""){
        try{
            var url = "http://"+serverIp+":"+port+"/keep_alive_pool/del?proxy="+JSON.stringify(proxy);
            request.get(url)
                .on("error",function(e){

                });
        }catch (e){

        }
    }
}