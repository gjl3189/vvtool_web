
/**
 * Module dependencies.
 */
var SyncProxy = require('./service/SyncProxy.js');
var express = require('express');
var routes = require('./routes');
var region = require('./routes/Region');
var usableProxyPool = require('./routes/UsableProxyPool');
var proxyPool = require('./routes/ProxyPool');
var App = require('./common/App.js');
var Notice = require('./service/Notice.js');
var ProxyDump = require('./service/ProxyDump.js');

var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', App.config.ServerPort);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.post('/region/parse', region.parse);
app.get('/region/setcache', region.setcache);
app.get('/region/getcache', region.getcache);

app.get('/usable_proxy_pool/add',usableProxyPool.add);
app.get('/usable_proxy_pool/del',usableProxyPool.del);
app.get('/usable_proxy_pool/getbest',usableProxyPool.getbest);
app.get('/usable_proxy_pool/getbest_and_exclude',usableProxyPool.getbestAndExclude);
app.get('/usable_proxy_pool/release',usableProxyPool.release);
app.get('/usable_proxy_pool/release_for_userid',usableProxyPool.releaseForUserid);
app.get('/usable_proxy_pool/all',usableProxyPool.all);
app.get('/usable_proxy_pool/fail',usableProxyPool.fail);
app.get('/usable_proxy_pool/save',function(req,res){
    ProxyDump.save();
    res.send(App.sendSuccess());
});

app.get('/proxy_pool/all',proxyPool.all);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


//把之前验证有效的代理载入
SyncProxy.loadProxy();

//10分钟同步一次代理ip
setInterval(function(){
    SyncProxy.start();
},1000*600);
SyncProxy.start();

//10分钟保存一次当前有效代理
setInterval(function(){
	ProxyDump.save();
},1000*600);

Notice.clientRestart();
