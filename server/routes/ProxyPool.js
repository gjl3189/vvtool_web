/**
 * 待验证的代理
 */

var ProxyPool = require("../service/ProxyPool.js"),
    App = require("../common/App.js")

exports.all = function(req, res){
    var list = ProxyPool.all();
    res.send(App.send(list));
}
