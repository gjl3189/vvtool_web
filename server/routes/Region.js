
/*
 * GET proxyregion.
 */
var ProxyRegion = require("../service/ProxyRegion.js");
var EventProxy = require("eventproxy");
var fs = require("fs");
var cacheRegionList = {};

exports.parse = function(req, res){
    var body = req.body.body;
	var region = {};
    if(body!=undefined){
		var ep = new EventProxy();
        ProxyRegion.start(ep,body);
		ep.on("end",function(region){
		    res.send(JSON.stringify(region));							
		});
		ep.on("timeout",function(){
		    res.send(JSON.stringify(region));							
		});
    }

};


exports.setcache = function(req, res){
    var host = req.query.host;
    var port = req.query.port;
    var region = req.query.region;;
    var key = host+":"+port;
    var result = "success";
    try{
        region = JSON.parse(region)
        cacheRegionList[key] = region;
    }catch (e){
        result = "fail";
    }
    res.send(result);
}

exports.getcache = function(req, res){
    var host = req.query.host;
    var port = req.query.port;
    //var region = req.query.region;
    var key = host+":"+port;
    var region = {};
    if(cacheRegionList[key]!=undefined){
        region = cacheRegionList[key];
    }
    res.send(JSON.stringify(region));
}