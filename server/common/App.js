var Util = require("../util/util.js");
module.exports = new App();

function App(){
    //单例模式
    if(typeof arguments.callee.instance === "object"){
        return arguments.callee.instance;
    }
    var specialConfig = require("./Config.js");
    var config  = {
        "proxy_tmp_save":__dirname+"/../runtime/data/proxy_tmp_save.json"
    }
    this.config = Util.merge(specialConfig,config);

    arguments.callee.instance = this;
}

App.prototype.hasServer = function(serverIp){
    var len = this.config.ServerList.length;
    for(var i=0;i<len;i++){
        if(this.config.ServerList[i].host==serverIp){
            return true;
        }
    }
    return false;
}

App.prototype.sendSuccess = function(){
    return this.send({status:1});
}

App.prototype.sendFail = function(error){
    if(error instanceof Object){
        error = error.toString();
    }
    return this.send({status:0,error:error});
}

App.prototype.send = function(content){
    var result = "";
    try{
        result = JSON.stringify(content);
    }catch (e){
        result = e.toString();
    }
    return result;
}
