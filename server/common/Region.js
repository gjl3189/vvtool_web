/**
 * 地区结构体
 * @constructor
 */
function Region(){
    this.id = 0;
    this.name = "";//广州市
    this.shortname = "",//广州
    this.pname = "",//广东省
    this.pid = 0;
}

module.exports = Region;