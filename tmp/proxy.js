
var ProxyInfo = require("./common/ProxyInfo.js");
var CheckProxy = require("./service/CheckProxy.js");
var ProxyPool = require("./service/UsableProxyPool.js");

var proxy = new ProxyInfo();
//proxy.host = "27.24.158.130";
//proxy.port = 80;
proxy.host = "localhost";
proxy.port = 8081;
proxy.regionId = 1;

var checkProxy =  new CheckProxy();
checkProxy.check(proxy);

setTimeout(function(){
    var proxyPool = new UsableProxyPool();
    var p = proxyPool.get(1);
    console.log(p);
},2000);

