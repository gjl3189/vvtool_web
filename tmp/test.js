
var ProxyInfo = require("./common/ProxyInfo.js");
var ProxyRegion = require("./service/ProxyRegion.js");

proxy = new ProxyInfo();
//proxy.host = '61.135.151.122';
//proxy.port = 80;
proxy.host = 'localhost';
proxy.port = 8081;
ProxyRegion.start(proxy).then(
    function(region){
        console.log(region)
    },
    function(err){
        console.log(err)
    }
);



/*
var ProxyPool = require("./service/ProxyPool.js");
var SyncProxy = require("./service/SyncProxy.js");

setInterval(function(){
    SyncProxy.start();
},2000);


setInterval(function(){
    console.log(ProxyPool.len());
},3000);
    */

/*
var ProxyInfo = require("./common/ProxyInfo.js"),
    CheckProxy = require("./service/CheckProxy.js"),
    UsableProxyPool = require("./service/UsableProxyPool.js");
    proxy = new ProxyInfo();

proxy.host = "localhost";
proxy.port = 808;
proxy.regionId = 1;

CheckProxy.check(proxy)
    .then(
        function(){
            console.log(UsableProxyPool.get(1));
        },
        function(err){
            console.log(err)
        });

*/