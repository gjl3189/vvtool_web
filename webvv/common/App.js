var Vvconfig = require("./Vvconfig.js");

module.exports = new App();

function App(){
    //单例模式
    if(typeof arguments.callee.instance === "object"){
        return arguments.callee.instance;
    }
    var region_host = "proxy2.vvtool.com";
    var region_port = 6001;
    //var region_host = "localhost";
    //var region_port = 1540;
    this.runtime = {
           expressApp:{}
    };
    this.config = {
        searchPath:"ssssss",
        default:{
            regionid:2,
            searchid:2,
            deviceid:1,
            browserid:0
        },
        region:{
            host:region_host,
            port:region_port,
            parse_region_url:"http://"+region_host+":"+region_port+"/region/parse",
            getcache_url:"http://"+region_host+":"+region_port+"/region/getcache",
            setcache_url:"http://"+region_host+":"+region_port+"/region/setcache",
			getproxy_url:"http://"+region_host+":"+region_port+"/usable_proxy_pool/getbest?region_id={regionId}",
            getproxy_and_exclude_url:"http://"+region_host+":"+region_port+"/usable_proxy_pool/getbest_and_exclude?region_id={regionId}&proxy={proxy}",
			release_proxy_url:"http://"+region_host+":"+region_port+"/usable_proxy_pool/release_for_userid?userid={userid}",
            fail_proxy_url:"http://"+region_host+":"+region_port+"/usable_proxy_pool/fail?proxy={proxy}"
        },
	ad:{},
        "tongji":{
            "normal":'var _hmt = _hmt || [];(function() {  var hm = document.createElement("script");  hm.src = "//hm.baidu.com/hm.js?4774a8cc0c635de717f0436e650de253";  var s = document.getElementsByTagName("script")[0];   s.parentNode.insertBefore(hm, s);})();',
            "timeout":'var _hmt = _hmt || []; (function() { var hm = document.createElement("script"); hm.src = "//hm.baidu.com/hm.js?d53dd62be617529a18b872cba4b998dc";  var s = document.getElementsByTagName("script")[0];   s.parentNode.insertBefore(hm, s);})();'
        },
        c_version:Vvconfig.c_version,
        //c_search_engine:Vvconfig.c_search_engine,
        c_device:Vvconfig.c_device,
        c_browser:Vvconfig.c_browser,
        //c_feature:Vvconfig.c_feature,
        preview_region:Vvconfig.preview_region,
        timeout:{
           search:8000//搜索请求超时时间
           //search:500//搜索请求超时时间
        }

    };
    arguments.callee.instance = this;
}

App.prototype.sendSuccess = function(){
    return this.send({status:1});
}

App.prototype.sendFail = function(error){
    if(error instanceof Object){
        error = error.toString();
    }
    return this.send({status:0,error:error});
}

App.prototype.send = function(content){
    var result = "";
    try{
        result = JSON.stringify(content);
    }catch (e){
        result = e.toString();
    }
    return result;
}
