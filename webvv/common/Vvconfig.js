module.exports = {
    c_browser:{
        1: {
            id: "1",
            agent: "",
            name: "IE6",
            deviceid: "1"
        },
        2: {
            id: "2",
            agent: "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) )",
            name: "IE7",
            deviceid: "1"
        },
        3: {
            id: "3",
            agent: "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) )",
            name: "IE8",
            deviceid: "1"
        },
        4: {
            id: "4",
            agent: "",
            name: "IE9",
            deviceid: "1"
        },
        5: {
            id: "5",
            agent: "",
            name: "IE10",
            deviceid: "1"
        },
        6: {
            id: "6",
            agent: "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36",
            name: "Chrome(谷歌)",
            deviceid: "1"
        },
        7: {
            id: "7",
            agent: "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; 360SE",
            name: "360浏览器",
            deviceid: "1"
        },
        8: {
            id: "8",
            agent: "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0",
            name: "Firefox(火狐)",
            deviceid: "1"
        },
        9: {
            id: "9",
            agent: "Mozilla/5.0 (Linux; Android 4.1.2; GT-I9300 Build/JZO54K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.90 Mobile Safari/537.36",
            name: "Chrome(谷歌)",
            deviceid: "2"
        },
        10: {
            id: "10",
            agent: "JUC (Linux; U; 2.1-update1; zh-cn; HERO CDMA; 320*480) UCWEB8.2.2.135/145/999",
            name: "360浏览器",
            deviceid: "2"
        },
        11: {
            id: "11",
            agent: "Opera/9.80 (Windows NT 5.1; U; Edition IBIS; zh-cn) Presto/2.10.229 Version/11.62",
            name: "Opera",
            deviceid: "1"
        },
        12: {
            id: "12",
            agent: "Mozilla/4.0 (compatible;Android;320x480)",
            name: "UC浏览器",
            deviceid: "2"
        },
        13: {
            id: "13",
            agent: "Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 MQQBrowser/4.3 Mobile Safari/533.1",
            name: "QQ浏览器",
            deviceid: "2"
        }
    },
    c_device:{
        1: {
            id: "1",
            name: "电脑端"
        },
        2: {
            id: "2",
            name: "移动端"
        }
    },
    /*
    c_search_engine:{
        1: {
            id: "1",
            name: "百度",
            aname: "baidu"
        },
        2: {
            id: "2",
            name: "360搜索",
            aname: "360"
        },
        3: {
            id: "3",
            name: "搜狗",
            aname: "sogou"
        },
        4: {
            id: "4",
            name: "搜搜",
            aname: "soso"
        }
    },
    */
    c_version:{
        1: {
            id: "1",
            name: "普通版",
            deviceid: "2",
            searchindex: "http://m.baidu.com/",
            searchargs: "word",
            engineid: "1",
            formsel: "form",
            pagesel: ".pagenav a",
            ismobile: "1",
            class: "BaiduWap",
            cacheindex: "BaiduWap",
            feature:['zhidao.baidu.com','m.baidu.com']
        },
        2: {
            id: "2",
            name: "普通版",
            deviceid: "1",
            searchindex: "http://www.baidu.com/",
            searchargs: "wd",
            engineid: "1",
            formsel: "#fm form",
            pagesel: "#page a",
            ismobile: "0",
            class: "BaiduPc",
            cacheindex: "BaiduPc",
            feature:['zhidao.baidu.com','m.baidu.com']
        },
        3: {
            id: "3",
            name: "普通版",
            deviceid: "1",
            searchindex: "http://www.so.com/",
            searchargs: "q",
            engineid: "2",
            formsel: "#search-box form",
            pagesel: "#page a",
            ismobile: "0",
            class: "360Pc",
            cacheindex: "360Pc",
            feature:['baike.so.com','ly.so.com','m.news.so.com','m.map.so.com']
        },
        4: {
            id: "4",
            name: "普通版",
            deviceid: "2",
            searchindex: "http://m.so.com/",
            searchargs: "q",
            engineid: "2",
            formsel: "#main form",
            pagesel: "#pagination a",
            ismobile: "1",
            class: "360M",
            cacheindex: "360M",
            feature:['baike.so.com','ly.so.com','m.news.so.com','m.map.so.com']
        },
        5: {
            id: "5",
            name: "普通版",
            deviceid: "1",
            searchindex: "http://www.sogou.com/",
            searchargs: "query",
            engineid: "3",
            formsel: "#searchForm",
            pagesel: "#pagebar_container a",
            ismobile: "0",
            class: "SogouPc",
            cacheindex: "SogouPc",
            feature:['wenwen.soso.com','wap.sogou.com']
        },
        6: {
            id: "6",
            name: "普通版",
            deviceid: "2",
            searchindex: "http://wap.sogou.com/",
            searchargs: "keyword",
            engineid: "3",
            formsel: ".bf",
            pagesel: ".pagenav a",
            ismobile: "1",
            class: "SogouWap",
            cacheindex: "SogouWap",
            feature:['wenwen.soso.com','wap.sogou.com']
        },
        7: {
            id: "7",
            name: "普通版",
            deviceid: "1",
            searchindex: "http://www.soso.com/",
            searchargs: "w",
            engineid: "4",
            formsel: "form",
            pagesel: "#pager a",
            ismobile: "0",
            class: "SosoPc",
            cacheindex: "SosoPc",
            feature:['wenwen.soso.com','wenwen.wap.soso.com']
        },
        8: {
            id: "8",
            name: "普通版",
            deviceid: "2",
            searchindex: "http://m.soso.com/",
            searchargs: "key",
            engineid: "4",
            formsel: "form",
            pagesel: ".mod_page a",
            ismobile: "1",
            class: "SosoM",
            cacheindex: "SosoM",
            feature:['wenwen.soso.com','wenwen.wap.soso.com']
        }
    },
    'preview_region':[
        {
            "id": "7",
            "name": "北京市",
            "capital": "北京",
            "seq": "147"
        },
        {
            "id": "2",
            "name": "浙江省",
            "capital": "杭州",
            "seq": "144"
        },
        {
            "id": "18",
            "name": "福建省",
            "capital": "福州",
            "seq": "141"
        },
        {
            "id": "9",
            "name": "江苏省",
            "capital": "南京",
            "seq": "138"
        },
        {
            "id": "15",
            "name": "广东省",
            "capital": "广州",
            "seq": "135"
        },
        {
            "id": "21",
            "name": "上海市",
            "capital": "上海",
            "seq": "132"
        },
        {
            "id": "11",
            "name": "辽宁省",
            "capital": "沈阳",
            "seq": "129"
        },
        {
            "id": "23",
            "name": "山东省",
            "capital": "济南",
            "seq": "126"
        },
        {
            "id": "54",
            "name": "陕西省",
            "capital": "西安",
            "seq": "123"
        },
        {
            "id": "5",
            "name": "四川省",
            "capital": "成都",
            "seq": "120"
        },
        {
            "id": "30",
            "name": "河北省",
            "capital": "石家庄",
            "seq": "117"
        },
        {
            "id": "100",
            "name": "山西省",
            "capital": "太原",
            "seq": "114"
        },
        {
            "id": "75",
            "name": "广西",
            "capital": "南宁",
            "seq": "111"
        },
        {
            "id": "34",
            "name": "重庆市",
            "capital": "重庆",
            "seq": "108"
        },
        {
            "id": "56",
            "name": "江西省",
            "capital": "南昌",
            "seq": "105"
        },
        {
            "id": "78",
            "name": "黑龙江省",
            "capital": "哈尔滨",
            "seq": "102"
        },
        {
            "id": "105",
            "name": "贵州省",
            "capital": "贵阳",
            "seq": "99"
        },
        {
            "id": "47",
            "name": "河南省",
            "capital": "郑州",
            "seq": "96"
        },
        {
            "id": "31",
            "name": "湖北省",
            "capital": "武汉",
            "seq": "93"
        },
        {
            "id": "42",
            "name": "天津市",
            "capital": "天津",
            "seq": "90"
        },
        {
            "id": "28",
            "name": "吉林省",
            "capital": "长春",
            "seq": "87"
        },
        {
            "id": "13",
            "name": "湖南省",
            "capital": "长沙",
            "seq": "84"
        },
        {
            "id": "26",
            "name": "安徽省",
            "capital": "合肥",
            "seq": "81"
        },
        {
            "id": "87",
            "name": "云南省",
            "capital": "昆明",
            "seq": "78"
        },
        {
            "id": "39",
            "name": "甘肃省",
            "capital": "兰州",
            "seq": "75"
        },
        {
            "id": "108",
            "name": "宁夏",
            "capital": "银川",
            "seq": "72"
        },
        {
            "id": "126",
            "name": "内蒙古",
            "capital": "呼和浩特",
            "seq": "69"
        },
        {
            "id": "36",
            "name": "新疆",
            "capital": "乌鲁木齐",
            "seq": "66"
        },
        {
            "id": "197",
            "name": "海南省",
            "capital": "海口市",
            "seq": "64"
        }
    ]
}
