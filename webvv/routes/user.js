var UserService = require("../service/UserService.js");
var userService = new UserService();
var SearchUrlService = require("../service/SearchUrlService.js");
var searchUrlService = new SearchUrlService();
var McryptService = require("../service/McryptService.js")
var mcryptService = new McryptService();

exports.login = function(req,res){
    res.render("login");
}

exports.loginPost = function(req,res){
    var map = {};
    map.email = req.body.email;
    map.password = req.body.password;

    var resResult = {};
    var index_url = "";

        userService.checkPassword(map)
        .then(
            function(result){
                if(result.status==1){
                    map.uid = result.uid;
                    var loginResult = userService.login(map);

                    index_url = result.index_url;
                    return searchUrlService.getUrlList(result.uid);
                }else{
                    resResult.status = 0;
                    resResult.msg = result.msg;
                    res.end(JSON.stringify(resResult));
               }
            },
            function(err){
                console.log(err);
            }
        )
        .then(
            function(searchUrlList){
                var use_url = "";
                var link = "";
                if(searchUrlList.length>0){
                    for(var i=0;i<searchUrlList.length;i++){
                        if(i>0){
                            link = "#";
                        }
                        use_url = use_url+link+searchUrlList[i]["url"];
                    }
                }
                res.setHeader( "Set-Cookie",["time="+loginResult.time,"email="+map.email,"sign="+loginResult.sign,"uidCrypt="+loginResult.uidCrypt,"use_url="+use_url,"index_url="+index_url] );

                resResult.status = 1;
                resResult.msg = "登录成功";
                res.end(JSON.stringify(resResult));
            },
            function(err){
                console.log(err);
            }
    )
        .then(function(){},function(err){
            console.log(err);
        })
}

exports.reg = function(req,res){
    var result = {};
    var map = {};
    map.email = req.body.email;
    map.password = req.body.password;

    userService.userExists(map.email)
    .then(
        function(exists){
            if(exists){
                result.status = 0;
                result.msg = "该邮箱已被注册";
                res.end(JSON.stringify(result));
                return false;
            }else{
                return true;
            }
        }
    )
    .then(
        function(rs){
            if(!rs){
                return rs;
            }
            return userService.reg(map);
        }
    )
    .then(
        function(uid){
            result.status = 1;
            result.msg = "注册成功";
            map.uid = uid;
            var loginResult = userService.login(map);
            res.setHeader( "Set-Cookie",["time="+loginResult.time,"email="+map.email,"sign="+loginResult.sign,"uidCrypt="+loginResult.uidCrypt,"use_url=","index_url="] );
            res.end(JSON.stringify(result));
        },
        function(){
            result.status = 0;
            result.msg = "注册失败";
            res.end(JSON.stringify(result));
        }
    );
}

exports.userExists = function(req,res){
    var result = {};
    userService.userExists(req.query.email).then(function(exists){
        if(exists==true){//存在
            res.end("false");
        }else{
            res.end("true");
        }
    },function(){
        res.end("false");
    })
}


exports.setIndexUrl = function(req,res){
    var uid = mcryptService.decode(req.cookies.uidCrypt);
    var url = req.query.url;
    userService.setIndexUrl(uid,url);

    res.setHeader( "Set-Cookie",["index_url="+url] );

    var resResult = {};
    resResult.msg = "设置成功";
    resResult.status = 1;
    res.end(JSON.stringify(resResult));
}