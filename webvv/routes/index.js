var Common = require("../service/Common.js"),
    App = require("../common/App.js"),
    SearchBase = require("../service/SearchBase.js"),
    UserService = require("../service/UserService.js"),
    userService = new UserService()

exports.index = function(req,res){
    userService.isLogin(req,res);

    var deviceBrowserList = {};
    var cBrowser = App.config.c_browser
    for(var i in cBrowser){
        var deviceid = cBrowser[i].deviceid;
        if( deviceBrowserList[deviceid] == undefined ){
            deviceBrowserList[deviceid] = [];
        }
        deviceBrowserList[deviceid].push({"id":cBrowser[i].id,"name":cBrowser[i].name});
    }

    var index_url = req.cookies.index_url;
    if(index_url==""){
        index_url = "/i";
    }else{
        index_url = "/"+App.config.searchPath+"?url="+index_url;
    }

    res.render('index',{
        "deviceBrowserList":JSON.stringify(deviceBrowserList),
        "regionlist":Common.regionList(),
        "regionOption":Common.regionOption(),
        "devOption":Common.devOption(),
        "browserOption":Common.browserOption(req,res),
        "searchOption":Common.searchOption(),

        "regionid":Common.getRegionid(req,res),
        "searchid":Common.getSearchid(req,res),
        "deviceid":Common.getDeviceid(req,res),
        "browserid":Common.getBrowserid(req,res),
	    "tongji_normal":App.config.tongji.normal,
        "searchPath":App.config.searchPath,
        "index_url":index_url
    });
}

exports.i = function(req,res){
    userService.isLogin(req,res);
    res.render("index_iframe",{
        "searchPath":App.config.searchPath
    });
}

exports.sss = function(req,res){
    this.s(req,res)
}

exports.s = function(req, res){
    userService.isLogin(req,res);
    var browserid = Common.getBrowserid(req,res);
    var searchid = Common.getSearchid(req,res);
    var deviceid = Common.getDeviceid(req,res);
    var regionid = Common.getRegionid(req,res);

    if(regionid==0 || searchid==0 || deviceid==0){
        res.send("非法请求 1");
        return false;
    }

    var searchBase = new SearchBase(req,res);
    searchBase.search()
    //res.render('index', { title:"test" });
    //res.render('index', { title:"dd" });
}

exports.searchinfo = function(req, res){
    var keyword = req.query.keyword
    if(keyword==undefined){
        keyword = "";
    }
    res.render("searchinfo",{
        "keyword":keyword
    })
}

exports.test = function(req,res){
    res.render("test");
}
