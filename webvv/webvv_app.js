var express = require('express');
    http = require('http'),
    path = require('path'),
    app = express(),
    index = require('./routes/index'),
    user = require('./routes/user'),
    App = require('./common/App.js'),
    SearchBase = require("./service/SearchBase.js"),
    UserService = require("./service/UserService.js"),
    userService = new UserService()

console.log(path.join(__dirname, 'views'));
// all environments
app.set('port', 808);
app.set('rootpath',__dirname);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
//app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(app.router);
// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get("/login",user.login);
app.post("/loginPost",user.loginPost);
app.get("/userExists",user.userExists);
app.post("/reg",user.reg);
app.get("/setIndexUrl",user.setIndexUrl);

app.get('/index.php',index.index);
app.get('/',index.index);
app.get('/i',index.i);
app.get('/s',index.s);
//app.get('/ss',index.s);
app.get('/'+App.config.searchPath , index.s)
app.get('/searchinfo',index.searchinfo);
app.get('/test',index.test);

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});

App.runtime.expressApp = app;
//setInterval(function(){
//    new SearchBase().allIndexHtml();
//},1000*60*60);
//new SearchBase().allIndexHtml();

app.render('tongji_normal',function(error,html){
	if(error!=null){
		console.log("统计代码 初始化出错:",error);
	}
	App.config.tongji.normal = html;
});

app.render('tongji_timeout',function(error,html){
	if(error!=null){
		console.log("统计代码(超时) 初始化出错:",error);
	}
        App.config.tongji.timeout = html;
});

app.render('ad',function(error,html){
        if(error!=null){
                console.log("广告代码 初始化出错:",error);
        }
        App.config.ad.dianxin = html;
});
