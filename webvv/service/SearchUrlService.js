var Q = require("q");
var MysqlService = require("./MysqlService.js");
var mysqlService = new MysqlService();
module.exports = SearchUrlService

function SearchUrlService(){

}

SearchUrlService.prototype.saveUrl = function(uid,url){
    var defered = Q.defer();
    var data = {}
    data.uid = uid;
    data.url = url;
    mysqlService.insertJson("search_url",data)
        .then(function(row){
            defered.resolve(true);
            console.log(row);
        },function(err){
            console.log(err);
            defered.reject(err);
        })
    return defered.promise;
}

SearchUrlService.prototype.getUrlList = function(uid){
    var defered = Q.defer();
    mysqlService.select("select * from search_url where uid="+uid)
        .then(function(rows){
            defered.resolve(rows);
        },function(err){
            defered.reject(err);
        })
    return defered.promise;
}
