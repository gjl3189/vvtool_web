var App = require("../common/App.js");

module.exports = new Common();

function Common(){

}

Common.prototype.getRegionid = function(req,res){
//console.log(req.cookies);
//console.log(req.headers);
    return this.get(req,res,"regionid");
}

Common.prototype.getBrowserid = function(req,res){
    return this.get(req,res,"browserid");
}

Common.prototype.getSearchid = function(req,res){
    return this.get(req,res,"searchid");
}

Common.prototype.getDeviceid = function(req,res){
    return this.get(req,res,"deviceid");
}

Common.prototype.get = function(req,res,key){
    var value = "";

    if(req.query!=undefined && req.query[key]){
	    value = req.query[key];
    }else if(value=="" && req.cookies!=undefined && req.cookies[key]){
        value = req.cookies[key];
    }else{
        value = App.config.default[key];
    }
    return value;
}

Common.prototype.regionList = function(){
    return App.config.preview_region;
}

Common.prototype.regionOption = function(){
    var regionList = this.regionList();
    var option = "";
    var len = regionList.length;
    for(var i=0;i<len;i++){
        var regionid = regionList[i].id;
        var name = regionList[i].name;
        option += "<option value='"+regionid+"'>"+name+"</option>"
    }
    return option;
}

Common.prototype.searchOption = function(){
    var c_search_engine  = App.config.c_search_engine;
    var option = "";
    for(var id in c_search_engine){
        option += "<option value='"+id+"'>"+c_search_engine[id].name+"</option>";
    }
    return option;
}

Common.prototype.browserOption = function(req,res){
    var c_browser = App.config.c_browser;
    var option = "<option value='0'>当前浏览器</option>";
    var deviceid = this.getDeviceid(req,res);
    for(var id in c_browser){
        if(deviceid==c_browser[id].deviceid){
            option += "<option value='"+id+"'>"+c_browser[id].name+"</option>";
        }
    }
    return option;
}

Common.prototype.devOption = function(){
    var c_device  = App.config.c_device;
    var option = "";
    for(var id in c_device){
        option += "<option value='"+id+"'>"+c_device[id].name+"</option>";
    }
    return option;
}