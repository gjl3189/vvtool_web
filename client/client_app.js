var CheckProxy2Pool = require("./service/CheckProxy2Pool.js"),
    ProxyPool = require("./service/ProxyPool.js"),
    ProxyInfo = require("./common/ProxyInfo.js"),
    SyncProxy = require("./service/SyncProxy.js");
    KeepAlivePool = require('./routes/KeepAlivePool'),
    express = require('express'),
    http = require('http'),
    path = require('path'),
    App = require('./common/App.js')

//10分钟同步一次待验证的代理ip
SyncProxy.start();
setInterval(function(){
    SyncProxy.start();
},1000*600);

var p = new ProxyInfo();
p.host = "119.188.9.115";
p.port = 80;
//ProxyPool.push(p);

//验证代理
setInterval(function(){
    CheckProxy2Pool.start();
},500);

//web 服务
var app = express();

app.set('port', App.config.appport);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/keep_alive_pool/del',KeepAlivePool.del);
app.get('/keep_alive_pool/all',KeepAlivePool.all);
app.get('/system/exit',function(req,res){
    res.send(App.sendSuccess());
    process.exit();
})

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});