var KeepAlivePool = require("../service/KeepAlivePool.js"),
    UsableProxyPool = require("../service/UsableProxyPool.js"),
    App = require("../common/App.js"),
    ProxyInfo = require("../common/ProxyInfo.js")
/**
* keepalive 删除
*/
exports.del = function(req, res){
	try{
		var p = req.query.proxy;
		p = JSON.parse(p);
		var proxy = new ProxyInfo(p);
        setTimeout(function(){
            KeepAlivePool.del(proxy);
        },1000);

		res.send(App.sendSuccess());
	}catch (e){
		res.send(App.sendFail(e));
	}				
};

exports.all = function(req, res){
    var list = {};
    list.keepalive  = KeepAlivePool.all();
    list.usable = UsableProxyPool.all();
    try{
        res.send(App.send(list));
    }catch (e){
        res.send(App.sendFail(e));
    }
}
