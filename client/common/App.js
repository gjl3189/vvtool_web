var util = require("../util/util.js");
module.exports = new App();

function App(){
    //单例模式
    if(typeof arguments.callee.instance === "object"){
        return arguments.callee.instance;
    }
    //var region_host = "proxy2.vvtool.com";
    //var region_port = 6001;
    this.config = require("./Config.js");
    var config = {
        stablePort : [80,8080,3128,9999,8090,8000,9000,8081,81,8888,8118] //比较稳定胡代理使用的端口
    }
    this.config = util.merge(this.config,config);

    arguments.callee.instance = this;
}

App.prototype.sendSuccess = function(){
    return this.send({status:1});
}

App.prototype.sendFail = function(error){
    if(error instanceof Object){
        error = error.toString();
    }
    return this.send({status:0,error:error});
}

App.prototype.send = function(content){
    var result = "";
    try{
        result = JSON.stringify(content);
    }catch (e){
        result = e.toString();
    }
    return result;
}

App.prototype.debug = function(log){
    console.debug(log);
}
