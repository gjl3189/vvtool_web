
module.exports = function(p){
	if(p==undefined){
		p = {};
	}
    this.host = p.host==undefined ? '' : p.host;
    this.port = p.port==undefined ? 0 : p.port;
    this.regionId = p.regionId==undefined ? 0 : p.regionId;
    this.region = p.region==undefined ? '' : p.region;
    this.resTime = p.resTime==undefined ? 0 : p.resTime; //响应时间
    this.resBodyTime = p.resBodyTime==undefined ? 0 : p.resBodyTime; //返回指定内容所需时间
    this.resBaiduTime = p.resBaiduTime==undefined ? 0 : p.resBaiduTime; //返回搜索所需时间
    this.weight = p.weight==undefined? 0 : p.weight; //权重 越低越好
}
