/**
 * 地区列表
 */


module.exports = {
	"1": {
		"id": "1",
		"pid": "0",
		"pname": "",
		"name": "中国",
		"shortname": "中国"
	},
	"2": {
		"id": "2",
		"pid": "1",
		"pname": "中国",
		"name": "浙江省",
		"shortname": "浙江"
	},
	"5": {
		"id": "5",
		"pid": "1",
		"pname": "中国",
		"name": "四川省",
		"shortname": "四川"
	},
	"7": {
		"id": "7",
		"pid": "1",
		"pname": "中国",
		"name": "北京市",
		"shortname": "北京"
	},
	"9": {
		"id": "9",
		"pid": "1",
		"pname": "中国",
		"name": "江苏省",
		"shortname": "江苏"
	},
	"11": {
		"id": "11",
		"pid": "1",
		"pname": "中国",
		"name": "辽宁省",
		"shortname": "辽宁"
	},
	"13": {
		"id": "13",
		"pid": "1",
		"pname": "中国",
		"name": "湖南省",
		"shortname": "湖南"
	},
	"15": {
		"id": "15",
		"pid": "1",
		"pname": "中国",
		"name": "广东省",
		"shortname": "广东"
	},
	"18": {
		"id": "18",
		"pid": "1",
		"pname": "中国",
		"name": "福建省",
		"shortname": "福建"
	},
	"21": {
		"id": "21",
		"pid": "1",
		"pname": "中国",
		"name": "上海市",
		"shortname": "上海"
	},
	"23": {
		"id": "23",
		"pid": "1",
		"pname": "中国",
		"name": "山东省",
		"shortname": "山东"
	},
	"26": {
		"id": "26",
		"pid": "1",
		"pname": "中国",
		"name": "安徽省",
		"shortname": "安徽"
	},
	"28": {
		"id": "28",
		"pid": "1",
		"pname": "中国",
		"name": "吉林省",
		"shortname": "吉林"
	},
	"30": {
		"id": "30",
		"pid": "1",
		"pname": "中国",
		"name": "河北省",
		"shortname": "河北"
	},
	"31": {
		"id": "31",
		"pid": "1",
		"pname": "中国",
		"name": "湖北省",
		"shortname": "湖北"
	},
	"34": {
		"id": "34",
		"pid": "1",
		"pname": "中国",
		"name": "重庆市",
		"shortname": "重庆"
	},
	"36": {
		"id": "36",
		"pid": "1",
		"pname": "中国",
		"name": "新疆",
		"shortname": "新疆"
	},
	"39": {
		"id": "39",
		"pid": "1",
		"pname": "中国",
		"name": "甘肃省",
		"shortname": "甘肃"
	},
	"42": {
		"id": "42",
		"pid": "1",
		"pname": "中国",
		"name": "天津市",
		"shortname": "天津"
	},
	"47": {
		"id": "47",
		"pid": "1",
		"pname": "中国",
		"name": "河南省",
		"shortname": "河南"
	},
	"54": {
		"id": "54",
		"pid": "1",
		"pname": "中国",
		"name": "陕西省",
		"shortname": "陕西"
	},
	"56": {
		"id": "56",
		"pid": "1",
		"pname": "中国",
		"name": "江西省",
		"shortname": "江西"
	},
	"75": {
		"id": "75",
		"pid": "1",
		"pname": "中国",
		"name": "广西",
		"shortname": "广西"
	},
	"78": {
		"id": "78",
		"pid": "1",
		"pname": "中国",
		"name": "黑龙江省",
		"shortname": "黑龙江"
	},
	"87": {
		"id": "87",
		"pid": "1",
		"pname": "中国",
		"name": "云南省",
		"shortname": "云南"
	},
	"100": {
		"id": "100",
		"pid": "1",
		"pname": "中国",
		"name": "山西省",
		"shortname": "山西"
	},
	"105": {
		"id": "105",
		"pid": "1",
		"pname": "中国",
		"name": "贵州省",
		"shortname": "贵州"
	},
	"108": {
		"id": "108",
		"pid": "1",
		"pname": "中国",
		"name": "宁夏",
		"shortname": "宁夏"
	},
	"126": {
		"id": "126",
		"pid": "1",
		"pname": "中国",
		"name": "内蒙古",
		"shortname": "内蒙古"
	},
	"197": {
		"id": "197",
		"pid": "1",
		"pname": "中国",
		"name": "海南省",
		"shortname": "海南"
	},
	"202": {
		"id": "202",
		"pid": "1",
		"pname": "中国",
		"name": "青海省",
		"shortname": "青海"
	},
	"209": {
		"id": "209",
		"pid": "2",
		"pname": "浙江省",
		"name": "杭州市",
		"shortname": "杭州"
	},
	"210": {
		"id": "210",
		"pid": "2",
		"pname": "浙江省",
		"name": "宁波市",
		"shortname": "宁波"
	},
	"211": {
		"id": "211",
		"pid": "2",
		"pname": "浙江省",
		"name": "温州市",
		"shortname": "温州"
	},
	"212": {
		"id": "212",
		"pid": "2",
		"pname": "浙江省",
		"name": "嘉兴市",
		"shortname": "嘉兴"
	},
	"213": {
		"id": "213",
		"pid": "2",
		"pname": "浙江省",
		"name": "湖州市",
		"shortname": "湖州"
	},
	"214": {
		"id": "214",
		"pid": "2",
		"pname": "浙江省",
		"name": "绍兴市",
		"shortname": "绍兴"
	},
	"215": {
		"id": "215",
		"pid": "2",
		"pname": "浙江省",
		"name": "金华市",
		"shortname": "金华"
	},
	"216": {
		"id": "216",
		"pid": "2",
		"pname": "浙江省",
		"name": "衢州市",
		"shortname": "衢州"
	},
	"217": {
		"id": "217",
		"pid": "2",
		"pname": "浙江省",
		"name": "舟山市",
		"shortname": "舟山"
	},
	"218": {
		"id": "218",
		"pid": "2",
		"pname": "浙江省",
		"name": "台州市",
		"shortname": "台州"
	},
	"219": {
		"id": "219",
		"pid": "2",
		"pname": "浙江省",
		"name": "丽水市",
		"shortname": "丽水"
	},
	"220": {
		"id": "220",
		"pid": "5",
		"pname": "四川省",
		"name": "成都市",
		"shortname": "成都"
	},
	"221": {
		"id": "221",
		"pid": "5",
		"pname": "四川省",
		"name": "自贡市",
		"shortname": "自贡"
	},
	"222": {
		"id": "222",
		"pid": "5",
		"pname": "四川省",
		"name": "攀枝花市",
		"shortname": "攀枝花"
	},
	"223": {
		"id": "223",
		"pid": "5",
		"pname": "四川省",
		"name": "泸州市",
		"shortname": "泸州"
	},
	"224": {
		"id": "224",
		"pid": "5",
		"pname": "四川省",
		"name": "德阳市",
		"shortname": "德阳"
	},
	"225": {
		"id": "225",
		"pid": "5",
		"pname": "四川省",
		"name": "绵阳市",
		"shortname": "绵阳"
	},
	"226": {
		"id": "226",
		"pid": "5",
		"pname": "四川省",
		"name": "广元市",
		"shortname": "广元"
	},
	"227": {
		"id": "227",
		"pid": "5",
		"pname": "四川省",
		"name": "遂宁市",
		"shortname": "遂宁"
	},
	"228": {
		"id": "228",
		"pid": "5",
		"pname": "四川省",
		"name": "内江市",
		"shortname": "内江"
	},
	"229": {
		"id": "229",
		"pid": "5",
		"pname": "四川省",
		"name": "乐山市",
		"shortname": "乐山"
	},
	"230": {
		"id": "230",
		"pid": "5",
		"pname": "四川省",
		"name": "南充市",
		"shortname": "南充"
	},
	"231": {
		"id": "231",
		"pid": "5",
		"pname": "四川省",
		"name": "眉山市",
		"shortname": "眉山"
	},
	"232": {
		"id": "232",
		"pid": "5",
		"pname": "四川省",
		"name": "宜宾市",
		"shortname": "宜宾"
	},
	"233": {
		"id": "233",
		"pid": "5",
		"pname": "四川省",
		"name": "广安市",
		"shortname": "广安"
	},
	"234": {
		"id": "234",
		"pid": "5",
		"pname": "四川省",
		"name": "达州市",
		"shortname": "达州"
	},
	"235": {
		"id": "235",
		"pid": "5",
		"pname": "四川省",
		"name": "雅安市",
		"shortname": "雅安"
	},
	"236": {
		"id": "236",
		"pid": "5",
		"pname": "四川省",
		"name": "巴中市",
		"shortname": "巴中"
	},
	"237": {
		"id": "237",
		"pid": "5",
		"pname": "四川省",
		"name": "资阳市",
		"shortname": "资阳"
	},
	"238": {
		"id": "238",
		"pid": "5",
		"pname": "四川省",
		"name": "阿坝州",
		"shortname": "阿坝州"
	},
	"239": {
		"id": "239",
		"pid": "5",
		"pname": "四川省",
		"name": "甘孜州",
		"shortname": "甘孜州"
	},
	"240": {
		"id": "240",
		"pid": "5",
		"pname": "四川省",
		"name": "凉山州",
		"shortname": "凉山州"
	},
	"241": {
		"id": "241",
		"pid": "7",
		"pname": "北京市",
		"name": "东城区",
		"shortname": "东城"
	},
	"242": {
		"id": "242",
		"pid": "7",
		"pname": "北京市",
		"name": "西城区",
		"shortname": "西城"
	},
	"243": {
		"id": "243",
		"pid": "7",
		"pname": "北京市",
		"name": "崇文区",
		"shortname": "崇文"
	},
	"244": {
		"id": "244",
		"pid": "7",
		"pname": "北京市",
		"name": "宣武区",
		"shortname": "宣武"
	},
	"245": {
		"id": "245",
		"pid": "7",
		"pname": "北京市",
		"name": "石景山区",
		"shortname": "石景山"
	},
	"246": {
		"id": "246",
		"pid": "7",
		"pname": "北京市",
		"name": "海淀区",
		"shortname": "海淀"
	},
	"247": {
		"id": "247",
		"pid": "7",
		"pname": "北京市",
		"name": "门头沟区",
		"shortname": "门头沟"
	},
	"248": {
		"id": "248",
		"pid": "7",
		"pname": "北京市",
		"name": "房山区",
		"shortname": "房山"
	},
	"249": {
		"id": "249",
		"pid": "7",
		"pname": "北京市",
		"name": "通州区",
		"shortname": "通州"
	},
	"250": {
		"id": "250",
		"pid": "7",
		"pname": "北京市",
		"name": "顺义区",
		"shortname": "顺义"
	},
	"251": {
		"id": "251",
		"pid": "7",
		"pname": "北京市",
		"name": "昌平区",
		"shortname": "昌平"
	},
	"252": {
		"id": "252",
		"pid": "7",
		"pname": "北京市",
		"name": "大兴区",
		"shortname": "大兴"
	},
	"253": {
		"id": "253",
		"pid": "7",
		"pname": "北京市",
		"name": "怀柔区",
		"shortname": "怀柔"
	},
	"254": {
		"id": "254",
		"pid": "7",
		"pname": "北京市",
		"name": "平谷区",
		"shortname": "平谷"
	},
	"255": {
		"id": "255",
		"pid": "7",
		"pname": "北京市",
		"name": "密云县",
		"shortname": "密云"
	},
	"256": {
		"id": "256",
		"pid": "7",
		"pname": "北京市",
		"name": "延庆县",
		"shortname": "延庆"
	},
	"257": {
		"id": "257",
		"pid": "9",
		"pname": "江苏省",
		"name": "南京市",
		"shortname": "南京"
	},
	"258": {
		"id": "258",
		"pid": "9",
		"pname": "江苏省",
		"name": "无锡市",
		"shortname": "无锡"
	},
	"259": {
		"id": "259",
		"pid": "9",
		"pname": "江苏省",
		"name": "徐州市",
		"shortname": "徐州"
	},
	"260": {
		"id": "260",
		"pid": "9",
		"pname": "江苏省",
		"name": "常州市",
		"shortname": "常州"
	},
	"261": {
		"id": "261",
		"pid": "9",
		"pname": "江苏省",
		"name": "苏州市",
		"shortname": "苏州"
	},
	"262": {
		"id": "262",
		"pid": "9",
		"pname": "江苏省",
		"name": "南通市",
		"shortname": "南通"
	},
	"263": {
		"id": "263",
		"pid": "9",
		"pname": "江苏省",
		"name": "连云港市",
		"shortname": "连云港"
	},
	"264": {
		"id": "264",
		"pid": "9",
		"pname": "江苏省",
		"name": "淮安市",
		"shortname": "淮安"
	},
	"265": {
		"id": "265",
		"pid": "9",
		"pname": "江苏省",
		"name": "盐城市",
		"shortname": "盐城"
	},
	"266": {
		"id": "266",
		"pid": "9",
		"pname": "江苏省",
		"name": "扬州市",
		"shortname": "扬州"
	},
	"267": {
		"id": "267",
		"pid": "9",
		"pname": "江苏省",
		"name": "镇江市",
		"shortname": "镇江"
	},
	"268": {
		"id": "268",
		"pid": "9",
		"pname": "江苏省",
		"name": "泰州市",
		"shortname": "泰州"
	},
	"269": {
		"id": "269",
		"pid": "9",
		"pname": "江苏省",
		"name": "宿迁市",
		"shortname": "宿迁"
	},
	"270": {
		"id": "270",
		"pid": "11",
		"pname": "辽宁省",
		"name": "沈阳市",
		"shortname": "沈阳"
	},
	"271": {
		"id": "271",
		"pid": "11",
		"pname": "辽宁省",
		"name": "大连市",
		"shortname": "大连"
	},
	"272": {
		"id": "272",
		"pid": "11",
		"pname": "辽宁省",
		"name": "鞍山市",
		"shortname": "鞍山"
	},
	"273": {
		"id": "273",
		"pid": "11",
		"pname": "辽宁省",
		"name": "抚顺市",
		"shortname": "抚顺"
	},
	"274": {
		"id": "274",
		"pid": "11",
		"pname": "辽宁省",
		"name": "本溪市",
		"shortname": "本溪"
	},
	"275": {
		"id": "275",
		"pid": "11",
		"pname": "辽宁省",
		"name": "丹东市",
		"shortname": "丹东"
	},
	"276": {
		"id": "276",
		"pid": "11",
		"pname": "辽宁省",
		"name": "锦州市",
		"shortname": "锦州"
	},
	"277": {
		"id": "277",
		"pid": "11",
		"pname": "辽宁省",
		"name": "营口市",
		"shortname": "营口"
	},
	"278": {
		"id": "278",
		"pid": "11",
		"pname": "辽宁省",
		"name": "阜新市",
		"shortname": "阜新"
	},
	"279": {
		"id": "279",
		"pid": "11",
		"pname": "辽宁省",
		"name": "辽阳市",
		"shortname": "辽阳"
	},
	"280": {
		"id": "280",
		"pid": "11",
		"pname": "辽宁省",
		"name": "盘锦市",
		"shortname": "盘锦"
	},
	"281": {
		"id": "281",
		"pid": "11",
		"pname": "辽宁省",
		"name": "铁岭市",
		"shortname": "铁岭"
	},
	"282": {
		"id": "282",
		"pid": "11",
		"pname": "辽宁省",
		"name": "朝阳市",
		"shortname": "朝阳"
	},
	"283": {
		"id": "283",
		"pid": "11",
		"pname": "辽宁省",
		"name": "葫芦岛市",
		"shortname": "葫芦岛"
	},
	"284": {
		"id": "284",
		"pid": "13",
		"pname": "湖南省",
		"name": "长沙市",
		"shortname": "长沙"
	},
	"285": {
		"id": "285",
		"pid": "13",
		"pname": "湖南省",
		"name": "株洲市",
		"shortname": "株洲"
	},
	"286": {
		"id": "286",
		"pid": "13",
		"pname": "湖南省",
		"name": "湘潭市",
		"shortname": "湘潭"
	},
	"287": {
		"id": "287",
		"pid": "13",
		"pname": "湖南省",
		"name": "衡阳市",
		"shortname": "衡阳"
	},
	"288": {
		"id": "288",
		"pid": "13",
		"pname": "湖南省",
		"name": "邵阳市",
		"shortname": "邵阳"
	},
	"289": {
		"id": "289",
		"pid": "13",
		"pname": "湖南省",
		"name": "岳阳市",
		"shortname": "岳阳"
	},
	"290": {
		"id": "290",
		"pid": "13",
		"pname": "湖南省",
		"name": "常德市",
		"shortname": "常德"
	},
	"291": {
		"id": "291",
		"pid": "13",
		"pname": "湖南省",
		"name": "张家界市",
		"shortname": "张家界"
	},
	"292": {
		"id": "292",
		"pid": "13",
		"pname": "湖南省",
		"name": "益阳市",
		"shortname": "益阳"
	},
	"293": {
		"id": "293",
		"pid": "13",
		"pname": "湖南省",
		"name": "郴州市",
		"shortname": "郴州"
	},
	"294": {
		"id": "294",
		"pid": "13",
		"pname": "湖南省",
		"name": "永州市",
		"shortname": "永州"
	},
	"295": {
		"id": "295",
		"pid": "13",
		"pname": "湖南省",
		"name": "怀化市",
		"shortname": "怀化"
	},
	"296": {
		"id": "296",
		"pid": "13",
		"pname": "湖南省",
		"name": "娄底市",
		"shortname": "娄底"
	},
	"297": {
		"id": "297",
		"pid": "13",
		"pname": "湖南省",
		"name": "湘西土家族苗族自治州",
		"shortname": "湘西土家族州"
	},
	"298": {
		"id": "298",
		"pid": "15",
		"pname": "广东省",
		"name": "广州市",
		"shortname": "广州"
	},
	"299": {
		"id": "299",
		"pid": "15",
		"pname": "广东省",
		"name": "韶关市",
		"shortname": "韶关"
	},
	"300": {
		"id": "300",
		"pid": "15",
		"pname": "广东省",
		"name": "深圳市",
		"shortname": "深圳"
	},
	"301": {
		"id": "301",
		"pid": "15",
		"pname": "广东省",
		"name": "珠海市",
		"shortname": "珠海"
	},
	"302": {
		"id": "302",
		"pid": "15",
		"pname": "广东省",
		"name": "汕头市",
		"shortname": "汕头"
	},
	"303": {
		"id": "303",
		"pid": "15",
		"pname": "广东省",
		"name": "佛山市",
		"shortname": "佛山"
	},
	"304": {
		"id": "304",
		"pid": "15",
		"pname": "广东省",
		"name": "江门市",
		"shortname": "江门"
	},
	"305": {
		"id": "305",
		"pid": "15",
		"pname": "广东省",
		"name": "湛江市",
		"shortname": "湛江"
	},
	"306": {
		"id": "306",
		"pid": "15",
		"pname": "广东省",
		"name": "茂名市",
		"shortname": "茂名"
	},
	"307": {
		"id": "307",
		"pid": "15",
		"pname": "广东省",
		"name": "肇庆市",
		"shortname": "肇庆"
	},
	"308": {
		"id": "308",
		"pid": "15",
		"pname": "广东省",
		"name": "惠州市",
		"shortname": "惠州"
	},
	"309": {
		"id": "309",
		"pid": "15",
		"pname": "广东省",
		"name": "梅州市",
		"shortname": "梅州"
	},
	"310": {
		"id": "310",
		"pid": "15",
		"pname": "广东省",
		"name": "汕尾市",
		"shortname": "汕尾"
	},
	"311": {
		"id": "311",
		"pid": "15",
		"pname": "广东省",
		"name": "河源市",
		"shortname": "河源"
	},
	"312": {
		"id": "312",
		"pid": "15",
		"pname": "广东省",
		"name": "阳江市",
		"shortname": "阳江"
	},
	"313": {
		"id": "313",
		"pid": "15",
		"pname": "广东省",
		"name": "清远市",
		"shortname": "清远"
	},
	"314": {
		"id": "314",
		"pid": "15",
		"pname": "广东省",
		"name": "东莞市",
		"shortname": "东莞"
	},
	"315": {
		"id": "315",
		"pid": "15",
		"pname": "广东省",
		"name": "中山市",
		"shortname": "中山"
	},
	"316": {
		"id": "316",
		"pid": "15",
		"pname": "广东省",
		"name": "潮州市",
		"shortname": "潮州"
	},
	"317": {
		"id": "317",
		"pid": "15",
		"pname": "广东省",
		"name": "揭阳市",
		"shortname": "揭阳"
	},
	"318": {
		"id": "318",
		"pid": "15",
		"pname": "广东省",
		"name": "云浮市",
		"shortname": "云浮"
	},
	"319": {
		"id": "319",
		"pid": "18",
		"pname": "福建省",
		"name": "福州市",
		"shortname": "福州"
	},
	"320": {
		"id": "320",
		"pid": "18",
		"pname": "福建省",
		"name": "厦门市",
		"shortname": "厦门"
	},
	"321": {
		"id": "321",
		"pid": "18",
		"pname": "福建省",
		"name": "莆田市",
		"shortname": "莆田"
	},
	"322": {
		"id": "322",
		"pid": "18",
		"pname": "福建省",
		"name": "三明市",
		"shortname": "三明"
	},
	"323": {
		"id": "323",
		"pid": "18",
		"pname": "福建省",
		"name": "泉州市",
		"shortname": "泉州"
	},
	"324": {
		"id": "324",
		"pid": "18",
		"pname": "福建省",
		"name": "漳州市",
		"shortname": "漳州"
	},
	"325": {
		"id": "325",
		"pid": "18",
		"pname": "福建省",
		"name": "南平市",
		"shortname": "南平"
	},
	"326": {
		"id": "326",
		"pid": "18",
		"pname": "福建省",
		"name": "龙岩市",
		"shortname": "龙岩"
	},
	"327": {
		"id": "327",
		"pid": "18",
		"pname": "福建省",
		"name": "宁德市",
		"shortname": "宁德"
	},
	"328": {
		"id": "328",
		"pid": "21",
		"pname": "上海市",
		"name": "黄浦区",
		"shortname": "黄浦"
	},
	"329": {
		"id": "329",
		"pid": "21",
		"pname": "上海市",
		"name": "卢湾区",
		"shortname": "卢湾"
	},
	"330": {
		"id": "330",
		"pid": "21",
		"pname": "上海市",
		"name": "徐汇区",
		"shortname": "徐汇"
	},
	"331": {
		"id": "331",
		"pid": "21",
		"pname": "上海市",
		"name": "长宁区",
		"shortname": "长宁"
	},
	"332": {
		"id": "332",
		"pid": "21",
		"pname": "上海市",
		"name": "静安区",
		"shortname": "静安"
	},
	"333": {
		"id": "333",
		"pid": "21",
		"pname": "上海市",
		"name": "闸北区",
		"shortname": "闸北"
	},
	"334": {
		"id": "334",
		"pid": "21",
		"pname": "上海市",
		"name": "虹口区",
		"shortname": "虹口"
	},
	"335": {
		"id": "335",
		"pid": "21",
		"pname": "上海市",
		"name": "杨浦区",
		"shortname": "杨浦"
	},
	"336": {
		"id": "336",
		"pid": "21",
		"pname": "上海市",
		"name": "闵行区",
		"shortname": "闵行"
	},
	"337": {
		"id": "337",
		"pid": "21",
		"pname": "上海市",
		"name": "嘉定区",
		"shortname": "嘉定"
	},
	"338": {
		"id": "338",
		"pid": "21",
		"pname": "上海市",
		"name": "浦东新区",
		"shortname": "浦东新"
	},
	"339": {
		"id": "339",
		"pid": "21",
		"pname": "上海市",
		"name": "金山区",
		"shortname": "金山"
	},
	"340": {
		"id": "340",
		"pid": "21",
		"pname": "上海市",
		"name": "松江区",
		"shortname": "松江"
	},
	"341": {
		"id": "341",
		"pid": "21",
		"pname": "上海市",
		"name": "青浦区",
		"shortname": "青浦"
	},
	"342": {
		"id": "342",
		"pid": "21",
		"pname": "上海市",
		"name": "南汇区",
		"shortname": "南汇"
	},
	"343": {
		"id": "343",
		"pid": "21",
		"pname": "上海市",
		"name": "奉贤区",
		"shortname": "奉贤"
	},
	"344": {
		"id": "344",
		"pid": "21",
		"pname": "上海市",
		"name": "崇明县",
		"shortname": "崇明"
	},
	"345": {
		"id": "345",
		"pid": "23",
		"pname": "山东省",
		"name": "济南市",
		"shortname": "济南"
	},
	"346": {
		"id": "346",
		"pid": "23",
		"pname": "山东省",
		"name": "青岛市",
		"shortname": "青岛"
	},
	"347": {
		"id": "347",
		"pid": "23",
		"pname": "山东省",
		"name": "淄博市",
		"shortname": "淄博"
	},
	"348": {
		"id": "348",
		"pid": "23",
		"pname": "山东省",
		"name": "枣庄市",
		"shortname": "枣庄"
	},
	"349": {
		"id": "349",
		"pid": "23",
		"pname": "山东省",
		"name": "东营市",
		"shortname": "东营"
	},
	"350": {
		"id": "350",
		"pid": "23",
		"pname": "山东省",
		"name": "烟台市",
		"shortname": "烟台"
	},
	"351": {
		"id": "351",
		"pid": "23",
		"pname": "山东省",
		"name": "潍坊市",
		"shortname": "潍坊"
	},
	"352": {
		"id": "352",
		"pid": "23",
		"pname": "山东省",
		"name": "济宁市",
		"shortname": "济宁"
	},
	"353": {
		"id": "353",
		"pid": "23",
		"pname": "山东省",
		"name": "泰安市",
		"shortname": "泰安"
	},
	"354": {
		"id": "354",
		"pid": "23",
		"pname": "山东省",
		"name": "威海市",
		"shortname": "威海"
	},
	"355": {
		"id": "355",
		"pid": "23",
		"pname": "山东省",
		"name": "日照市",
		"shortname": "日照"
	},
	"356": {
		"id": "356",
		"pid": "23",
		"pname": "山东省",
		"name": "莱芜市",
		"shortname": "莱芜"
	},
	"357": {
		"id": "357",
		"pid": "23",
		"pname": "山东省",
		"name": "临沂市",
		"shortname": "临沂"
	},
	"358": {
		"id": "358",
		"pid": "23",
		"pname": "山东省",
		"name": "德州市",
		"shortname": "德州"
	},
	"359": {
		"id": "359",
		"pid": "23",
		"pname": "山东省",
		"name": "聊城市",
		"shortname": "聊城"
	},
	"360": {
		"id": "360",
		"pid": "23",
		"pname": "山东省",
		"name": "滨州市",
		"shortname": "滨州"
	},
	"361": {
		"id": "361",
		"pid": "23",
		"pname": "山东省",
		"name": "荷泽市",
		"shortname": "荷泽"
	},
	"362": {
		"id": "362",
		"pid": "26",
		"pname": "安徽省",
		"name": "合肥市",
		"shortname": "合肥"
	},
	"363": {
		"id": "363",
		"pid": "26",
		"pname": "安徽省",
		"name": "芜湖市",
		"shortname": "芜湖"
	},
	"364": {
		"id": "364",
		"pid": "26",
		"pname": "安徽省",
		"name": "蚌埠市",
		"shortname": "蚌埠"
	},
	"365": {
		"id": "365",
		"pid": "26",
		"pname": "安徽省",
		"name": "淮南市",
		"shortname": "淮南"
	},
	"366": {
		"id": "366",
		"pid": "26",
		"pname": "安徽省",
		"name": "马鞍山市",
		"shortname": "马鞍山"
	},
	"367": {
		"id": "367",
		"pid": "26",
		"pname": "安徽省",
		"name": "淮北市",
		"shortname": "淮北"
	},
	"368": {
		"id": "368",
		"pid": "26",
		"pname": "安徽省",
		"name": "铜陵市",
		"shortname": "铜陵"
	},
	"369": {
		"id": "369",
		"pid": "26",
		"pname": "安徽省",
		"name": "安庆市",
		"shortname": "安庆"
	},
	"370": {
		"id": "370",
		"pid": "26",
		"pname": "安徽省",
		"name": "黄山市",
		"shortname": "黄山"
	},
	"371": {
		"id": "371",
		"pid": "26",
		"pname": "安徽省",
		"name": "滁州市",
		"shortname": "滁州"
	},
	"372": {
		"id": "372",
		"pid": "26",
		"pname": "安徽省",
		"name": "阜阳市",
		"shortname": "阜阳"
	},
	"373": {
		"id": "373",
		"pid": "26",
		"pname": "安徽省",
		"name": "宿州市",
		"shortname": "宿州"
	},
	"374": {
		"id": "374",
		"pid": "26",
		"pname": "安徽省",
		"name": "巢湖市",
		"shortname": "巢湖"
	},
	"375": {
		"id": "375",
		"pid": "26",
		"pname": "安徽省",
		"name": "六安市",
		"shortname": "六安"
	},
	"376": {
		"id": "376",
		"pid": "26",
		"pname": "安徽省",
		"name": "亳州市",
		"shortname": "亳州"
	},
	"377": {
		"id": "377",
		"pid": "26",
		"pname": "安徽省",
		"name": "池州市",
		"shortname": "池州"
	},
	"378": {
		"id": "378",
		"pid": "26",
		"pname": "安徽省",
		"name": "宣城市",
		"shortname": "宣城"
	},
	"379": {
		"id": "379",
		"pid": "28",
		"pname": "吉林省",
		"name": "长春市",
		"shortname": "长春"
	},
	"380": {
		"id": "380",
		"pid": "28",
		"pname": "吉林省",
		"name": "吉林市",
		"shortname": "吉林"
	},
	"381": {
		"id": "381",
		"pid": "28",
		"pname": "吉林省",
		"name": "四平市",
		"shortname": "四平"
	},
	"382": {
		"id": "382",
		"pid": "28",
		"pname": "吉林省",
		"name": "辽源市",
		"shortname": "辽源"
	},
	"383": {
		"id": "383",
		"pid": "28",
		"pname": "吉林省",
		"name": "通化市",
		"shortname": "通化"
	},
	"384": {
		"id": "384",
		"pid": "28",
		"pname": "吉林省",
		"name": "白山市",
		"shortname": "白山"
	},
	"385": {
		"id": "385",
		"pid": "28",
		"pname": "吉林省",
		"name": "松原市",
		"shortname": "松原"
	},
	"386": {
		"id": "386",
		"pid": "28",
		"pname": "吉林省",
		"name": "白城市",
		"shortname": "白城"
	},
	"387": {
		"id": "387",
		"pid": "28",
		"pname": "吉林省",
		"name": "延边",
		"shortname": "延边"
	},
	"388": {
		"id": "388",
		"pid": "30",
		"pname": "河北省",
		"name": "石家庄市",
		"shortname": "石家庄"
	},
	"389": {
		"id": "389",
		"pid": "30",
		"pname": "河北省",
		"name": "唐山市",
		"shortname": "唐山"
	},
	"390": {
		"id": "390",
		"pid": "30",
		"pname": "河北省",
		"name": "秦皇岛市",
		"shortname": "秦皇岛"
	},
	"391": {
		"id": "391",
		"pid": "30",
		"pname": "河北省",
		"name": "邯郸市",
		"shortname": "邯郸"
	},
	"392": {
		"id": "392",
		"pid": "30",
		"pname": "河北省",
		"name": "邢台市",
		"shortname": "邢台"
	},
	"393": {
		"id": "393",
		"pid": "30",
		"pname": "河北省",
		"name": "保定市",
		"shortname": "保定"
	},
	"394": {
		"id": "394",
		"pid": "30",
		"pname": "河北省",
		"name": "张家口市",
		"shortname": "张家口"
	},
	"395": {
		"id": "395",
		"pid": "30",
		"pname": "河北省",
		"name": "承德市",
		"shortname": "承德"
	},
	"396": {
		"id": "396",
		"pid": "30",
		"pname": "河北省",
		"name": "沧州市",
		"shortname": "沧州"
	},
	"397": {
		"id": "397",
		"pid": "30",
		"pname": "河北省",
		"name": "廊坊市",
		"shortname": "廊坊"
	},
	"398": {
		"id": "398",
		"pid": "30",
		"pname": "河北省",
		"name": "衡水市",
		"shortname": "衡水"
	},
	"399": {
		"id": "399",
		"pid": "31",
		"pname": "湖北省",
		"name": "武汉市",
		"shortname": "武汉"
	},
	"400": {
		"id": "400",
		"pid": "31",
		"pname": "湖北省",
		"name": "黄石市",
		"shortname": "黄石"
	},
	"401": {
		"id": "401",
		"pid": "31",
		"pname": "湖北省",
		"name": "十堰市",
		"shortname": "十堰"
	},
	"402": {
		"id": "402",
		"pid": "31",
		"pname": "湖北省",
		"name": "宜昌市",
		"shortname": "宜昌"
	},
	"403": {
		"id": "403",
		"pid": "31",
		"pname": "湖北省",
		"name": "襄樊市",
		"shortname": "襄樊"
	},
	"404": {
		"id": "404",
		"pid": "31",
		"pname": "湖北省",
		"name": "鄂州市",
		"shortname": "鄂州"
	},
	"405": {
		"id": "405",
		"pid": "31",
		"pname": "湖北省",
		"name": "荆门市",
		"shortname": "荆门"
	},
	"406": {
		"id": "406",
		"pid": "31",
		"pname": "湖北省",
		"name": "孝感市",
		"shortname": "孝感"
	},
	"407": {
		"id": "407",
		"pid": "31",
		"pname": "湖北省",
		"name": "荆州市",
		"shortname": "荆州"
	},
	"408": {
		"id": "408",
		"pid": "31",
		"pname": "湖北省",
		"name": "黄冈市",
		"shortname": "黄冈"
	},
	"409": {
		"id": "409",
		"pid": "31",
		"pname": "湖北省",
		"name": "咸宁市",
		"shortname": "咸宁"
	},
	"410": {
		"id": "410",
		"pid": "31",
		"pname": "湖北省",
		"name": "随州市",
		"shortname": "随州"
	},
	"411": {
		"id": "411",
		"pid": "31",
		"pname": "湖北省",
		"name": "恩施土家族苗族自治州",
		"shortname": "恩施土家族州"
	},
	"412": {
		"id": "412",
		"pid": "31",
		"pname": "湖北省",
		"name": "仙桃市",
		"shortname": "仙桃"
	},
	"413": {
		"id": "413",
		"pid": "31",
		"pname": "湖北省",
		"name": "潜江市",
		"shortname": "潜江"
	},
	"414": {
		"id": "414",
		"pid": "31",
		"pname": "湖北省",
		"name": "天门市",
		"shortname": "天门"
	},
	"415": {
		"id": "415",
		"pid": "31",
		"pname": "湖北省",
		"name": "神农架林区",
		"shortname": "神农架林"
	},
	"416": {
		"id": "416",
		"pid": "34",
		"pname": "重庆市",
		"name": "万州区",
		"shortname": "万州"
	},
	"417": {
		"id": "417",
		"pid": "34",
		"pname": "重庆市",
		"name": "涪陵区",
		"shortname": "涪陵"
	},
	"418": {
		"id": "418",
		"pid": "34",
		"pname": "重庆市",
		"name": "渝中区",
		"shortname": "渝中"
	},
	"419": {
		"id": "419",
		"pid": "34",
		"pname": "重庆市",
		"name": "大渡口区",
		"shortname": "大渡口"
	},
	"420": {
		"id": "420",
		"pid": "34",
		"pname": "重庆市",
		"name": "沙坪坝区",
		"shortname": "沙坪坝"
	},
	"421": {
		"id": "421",
		"pid": "34",
		"pname": "重庆市",
		"name": "九龙坡区",
		"shortname": "九龙坡"
	},
	"422": {
		"id": "422",
		"pid": "34",
		"pname": "重庆市",
		"name": "南岸区",
		"shortname": "南岸"
	},
	"423": {
		"id": "423",
		"pid": "34",
		"pname": "重庆市",
		"name": "北碚区",
		"shortname": "北碚"
	},
	"424": {
		"id": "424",
		"pid": "34",
		"pname": "重庆市",
		"name": "万盛区",
		"shortname": "万盛"
	},
	"425": {
		"id": "425",
		"pid": "34",
		"pname": "重庆市",
		"name": "渝北区",
		"shortname": "渝北"
	},
	"426": {
		"id": "426",
		"pid": "34",
		"pname": "重庆市",
		"name": "巴南区",
		"shortname": "巴南"
	},
	"427": {
		"id": "427",
		"pid": "34",
		"pname": "重庆市",
		"name": "黔江区",
		"shortname": "黔江"
	},
	"428": {
		"id": "428",
		"pid": "34",
		"pname": "重庆市",
		"name": "长寿区",
		"shortname": "长寿"
	},
	"429": {
		"id": "429",
		"pid": "34",
		"pname": "重庆市",
		"name": "綦江县",
		"shortname": "綦江"
	},
	"430": {
		"id": "430",
		"pid": "34",
		"pname": "重庆市",
		"name": "潼南县",
		"shortname": "潼南"
	},
	"431": {
		"id": "431",
		"pid": "34",
		"pname": "重庆市",
		"name": "铜梁县",
		"shortname": "铜梁"
	},
	"432": {
		"id": "432",
		"pid": "34",
		"pname": "重庆市",
		"name": "大足县",
		"shortname": "大足"
	},
	"433": {
		"id": "433",
		"pid": "34",
		"pname": "重庆市",
		"name": "荣昌县",
		"shortname": "荣昌"
	},
	"434": {
		"id": "434",
		"pid": "34",
		"pname": "重庆市",
		"name": "璧山县",
		"shortname": "璧山"
	},
	"435": {
		"id": "435",
		"pid": "34",
		"pname": "重庆市",
		"name": "梁平县",
		"shortname": "梁平"
	},
	"436": {
		"id": "436",
		"pid": "34",
		"pname": "重庆市",
		"name": "城口县",
		"shortname": "城口"
	},
	"437": {
		"id": "437",
		"pid": "34",
		"pname": "重庆市",
		"name": "丰都县",
		"shortname": "丰都"
	},
	"438": {
		"id": "438",
		"pid": "34",
		"pname": "重庆市",
		"name": "垫江县",
		"shortname": "垫江"
	},
	"439": {
		"id": "439",
		"pid": "34",
		"pname": "重庆市",
		"name": "武隆县",
		"shortname": "武隆"
	},
	"440": {
		"id": "440",
		"pid": "34",
		"pname": "重庆市",
		"name": "忠县",
		"shortname": "忠"
	},
	"441": {
		"id": "441",
		"pid": "34",
		"pname": "重庆市",
		"name": "开县",
		"shortname": "开"
	},
	"442": {
		"id": "442",
		"pid": "34",
		"pname": "重庆市",
		"name": "云阳县",
		"shortname": "云阳"
	},
	"443": {
		"id": "443",
		"pid": "34",
		"pname": "重庆市",
		"name": "奉节县",
		"shortname": "奉节"
	},
	"444": {
		"id": "444",
		"pid": "34",
		"pname": "重庆市",
		"name": "巫山县",
		"shortname": "巫山"
	},
	"445": {
		"id": "445",
		"pid": "34",
		"pname": "重庆市",
		"name": "巫溪县",
		"shortname": "巫溪"
	},
	"446": {
		"id": "446",
		"pid": "34",
		"pname": "重庆市",
		"name": "石柱县",
		"shortname": "石柱"
	},
	"447": {
		"id": "447",
		"pid": "34",
		"pname": "重庆市",
		"name": "秀山县",
		"shortname": "秀山"
	},
	"448": {
		"id": "448",
		"pid": "34",
		"pname": "重庆市",
		"name": "酉阳县",
		"shortname": "酉阳"
	},
	"449": {
		"id": "449",
		"pid": "34",
		"pname": "重庆市",
		"name": "彭水县",
		"shortname": "彭水"
	},
	"450": {
		"id": "450",
		"pid": "34",
		"pname": "重庆市",
		"name": "江津区",
		"shortname": "江津"
	},
	"451": {
		"id": "451",
		"pid": "34",
		"pname": "重庆市",
		"name": "合川区",
		"shortname": "合川"
	},
	"452": {
		"id": "452",
		"pid": "34",
		"pname": "重庆市",
		"name": "永川区",
		"shortname": "永川"
	},
	"453": {
		"id": "453",
		"pid": "34",
		"pname": "重庆市",
		"name": "南川区",
		"shortname": "南川"
	},
	"454": {
		"id": "454",
		"pid": "36",
		"pname": "新疆",
		"name": "乌鲁木齐市",
		"shortname": "乌鲁木齐"
	},
	"455": {
		"id": "455",
		"pid": "36",
		"pname": "新疆",
		"name": "克拉玛依市",
		"shortname": "克拉玛依"
	},
	"456": {
		"id": "456",
		"pid": "36",
		"pname": "新疆",
		"name": "吐鲁番地区",
		"shortname": "吐鲁番"
	},
	"457": {
		"id": "457",
		"pid": "36",
		"pname": "新疆",
		"name": "哈密地区",
		"shortname": "哈密"
	},
	"458": {
		"id": "458",
		"pid": "36",
		"pname": "新疆",
		"name": "昌吉州",
		"shortname": "昌吉州"
	},
	"459": {
		"id": "459",
		"pid": "36",
		"pname": "新疆",
		"name": "博尔州",
		"shortname": "博尔州"
	},
	"460": {
		"id": "460",
		"pid": "36",
		"pname": "新疆",
		"name": "巴音郭楞州",
		"shortname": "巴音郭楞州"
	},
	"461": {
		"id": "461",
		"pid": "36",
		"pname": "新疆",
		"name": "阿克苏地区",
		"shortname": "阿克苏"
	},
	"462": {
		"id": "462",
		"pid": "36",
		"pname": "新疆",
		"name": "克孜勒苏柯尔克孜自治州",
		"shortname": "克孜勒苏柯尔克孜州"
	},
	"463": {
		"id": "463",
		"pid": "36",
		"pname": "新疆",
		"name": "喀什地区",
		"shortname": "喀什"
	},
	"464": {
		"id": "464",
		"pid": "36",
		"pname": "新疆",
		"name": "和田地区",
		"shortname": "和田"
	},
	"465": {
		"id": "465",
		"pid": "36",
		"pname": "新疆",
		"name": "伊犁州",
		"shortname": "伊犁州"
	},
	"466": {
		"id": "466",
		"pid": "36",
		"pname": "新疆",
		"name": "塔城地区",
		"shortname": "塔城"
	},
	"467": {
		"id": "467",
		"pid": "36",
		"pname": "新疆",
		"name": "阿勒泰地区",
		"shortname": "阿勒泰"
	},
	"468": {
		"id": "468",
		"pid": "36",
		"pname": "新疆",
		"name": "石河子市",
		"shortname": "石河子"
	},
	"469": {
		"id": "469",
		"pid": "36",
		"pname": "新疆",
		"name": "阿拉尔市",
		"shortname": "阿拉尔"
	},
	"470": {
		"id": "470",
		"pid": "36",
		"pname": "新疆",
		"name": "图木舒克市",
		"shortname": "图木舒克"
	},
	"471": {
		"id": "471",
		"pid": "36",
		"pname": "新疆",
		"name": "五家渠市",
		"shortname": "五家渠"
	},
	"472": {
		"id": "472",
		"pid": "39",
		"pname": "甘肃省",
		"name": "兰州市",
		"shortname": "兰州"
	},
	"473": {
		"id": "473",
		"pid": "39",
		"pname": "甘肃省",
		"name": "嘉峪关市",
		"shortname": "嘉峪关"
	},
	"474": {
		"id": "474",
		"pid": "39",
		"pname": "甘肃省",
		"name": "金昌市",
		"shortname": "金昌"
	},
	"475": {
		"id": "475",
		"pid": "39",
		"pname": "甘肃省",
		"name": "白银市",
		"shortname": "白银"
	},
	"476": {
		"id": "476",
		"pid": "39",
		"pname": "甘肃省",
		"name": "天水市",
		"shortname": "天水"
	},
	"477": {
		"id": "477",
		"pid": "39",
		"pname": "甘肃省",
		"name": "武威市",
		"shortname": "武威"
	},
	"478": {
		"id": "478",
		"pid": "39",
		"pname": "甘肃省",
		"name": "张掖市",
		"shortname": "张掖"
	},
	"479": {
		"id": "479",
		"pid": "39",
		"pname": "甘肃省",
		"name": "平凉市",
		"shortname": "平凉"
	},
	"480": {
		"id": "480",
		"pid": "39",
		"pname": "甘肃省",
		"name": "酒泉市",
		"shortname": "酒泉"
	},
	"481": {
		"id": "481",
		"pid": "39",
		"pname": "甘肃省",
		"name": "庆阳市",
		"shortname": "庆阳"
	},
	"482": {
		"id": "482",
		"pid": "39",
		"pname": "甘肃省",
		"name": "定西市",
		"shortname": "定西"
	},
	"483": {
		"id": "483",
		"pid": "39",
		"pname": "甘肃省",
		"name": "陇南市",
		"shortname": "陇南"
	},
	"484": {
		"id": "484",
		"pid": "39",
		"pname": "甘肃省",
		"name": "临夏州",
		"shortname": "临夏州"
	},
	"485": {
		"id": "485",
		"pid": "39",
		"pname": "甘肃省",
		"name": "甘州",
		"shortname": "甘州"
	},
	"486": {
		"id": "486",
		"pid": "42",
		"pname": "天津市",
		"name": "河西区",
		"shortname": "河西"
	},
	"487": {
		"id": "487",
		"pid": "42",
		"pname": "天津市",
		"name": "南开区",
		"shortname": "南开"
	},
	"488": {
		"id": "488",
		"pid": "42",
		"pname": "天津市",
		"name": "河北区",
		"shortname": "河北"
	},
	"489": {
		"id": "489",
		"pid": "42",
		"pname": "天津市",
		"name": "红桥区",
		"shortname": "红桥"
	},
	"490": {
		"id": "490",
		"pid": "42",
		"pname": "天津市",
		"name": "塘沽区",
		"shortname": "塘沽"
	},
	"491": {
		"id": "491",
		"pid": "42",
		"pname": "天津市",
		"name": "汉沽区",
		"shortname": "汉沽"
	},
	"492": {
		"id": "492",
		"pid": "42",
		"pname": "天津市",
		"name": "大港区",
		"shortname": "大港"
	},
	"493": {
		"id": "493",
		"pid": "42",
		"pname": "天津市",
		"name": "东丽区",
		"shortname": "东丽"
	},
	"494": {
		"id": "494",
		"pid": "42",
		"pname": "天津市",
		"name": "西青区",
		"shortname": "西青"
	},
	"495": {
		"id": "495",
		"pid": "42",
		"pname": "天津市",
		"name": "津南区",
		"shortname": "津南"
	},
	"496": {
		"id": "496",
		"pid": "42",
		"pname": "天津市",
		"name": "北辰区",
		"shortname": "北辰"
	},
	"497": {
		"id": "497",
		"pid": "42",
		"pname": "天津市",
		"name": "武清区",
		"shortname": "武清"
	},
	"498": {
		"id": "498",
		"pid": "42",
		"pname": "天津市",
		"name": "宝坻区",
		"shortname": "宝坻"
	},
	"499": {
		"id": "499",
		"pid": "42",
		"pname": "天津市",
		"name": "宁河县",
		"shortname": "宁河"
	},
	"500": {
		"id": "500",
		"pid": "42",
		"pname": "天津市",
		"name": "静海县",
		"shortname": "静海"
	},
	"501": {
		"id": "501",
		"pid": "42",
		"pname": "天津市",
		"name": "蓟县",
		"shortname": "蓟"
	},
	"502": {
		"id": "502",
		"pid": "47",
		"pname": "河南省",
		"name": "郑州市",
		"shortname": "郑州"
	},
	"503": {
		"id": "503",
		"pid": "47",
		"pname": "河南省",
		"name": "开封市",
		"shortname": "开封"
	},
	"504": {
		"id": "504",
		"pid": "47",
		"pname": "河南省",
		"name": "洛阳市",
		"shortname": "洛阳"
	},
	"505": {
		"id": "505",
		"pid": "47",
		"pname": "河南省",
		"name": "平顶山市",
		"shortname": "平顶山"
	},
	"506": {
		"id": "506",
		"pid": "47",
		"pname": "河南省",
		"name": "安阳市",
		"shortname": "安阳"
	},
	"507": {
		"id": "507",
		"pid": "47",
		"pname": "河南省",
		"name": "鹤壁市",
		"shortname": "鹤壁"
	},
	"508": {
		"id": "508",
		"pid": "47",
		"pname": "河南省",
		"name": "新乡市",
		"shortname": "新乡"
	},
	"509": {
		"id": "509",
		"pid": "47",
		"pname": "河南省",
		"name": "焦作市",
		"shortname": "焦作"
	},
	"510": {
		"id": "510",
		"pid": "47",
		"pname": "河南省",
		"name": "濮阳市",
		"shortname": "濮阳"
	},
	"511": {
		"id": "511",
		"pid": "47",
		"pname": "河南省",
		"name": "许昌市",
		"shortname": "许昌"
	},
	"512": {
		"id": "512",
		"pid": "47",
		"pname": "河南省",
		"name": "漯河市",
		"shortname": "漯河"
	},
	"513": {
		"id": "513",
		"pid": "47",
		"pname": "河南省",
		"name": "三门峡市",
		"shortname": "三门峡"
	},
	"514": {
		"id": "514",
		"pid": "47",
		"pname": "河南省",
		"name": "南阳市",
		"shortname": "南阳"
	},
	"515": {
		"id": "515",
		"pid": "47",
		"pname": "河南省",
		"name": "商丘市",
		"shortname": "商丘"
	},
	"516": {
		"id": "516",
		"pid": "47",
		"pname": "河南省",
		"name": "信阳市",
		"shortname": "信阳"
	},
	"517": {
		"id": "517",
		"pid": "47",
		"pname": "河南省",
		"name": "周口市",
		"shortname": "周口"
	},
	"518": {
		"id": "518",
		"pid": "47",
		"pname": "河南省",
		"name": "驻马店市",
		"shortname": "驻马店"
	},
	"519": {
		"id": "519",
		"pid": "54",
		"pname": "陕西省",
		"name": "西安市",
		"shortname": "西安"
	},
	"520": {
		"id": "520",
		"pid": "54",
		"pname": "陕西省",
		"name": "铜川市",
		"shortname": "铜川"
	},
	"521": {
		"id": "521",
		"pid": "54",
		"pname": "陕西省",
		"name": "宝鸡市",
		"shortname": "宝鸡"
	},
	"522": {
		"id": "522",
		"pid": "54",
		"pname": "陕西省",
		"name": "咸阳市",
		"shortname": "咸阳"
	},
	"523": {
		"id": "523",
		"pid": "54",
		"pname": "陕西省",
		"name": "渭南市",
		"shortname": "渭南"
	},
	"524": {
		"id": "524",
		"pid": "54",
		"pname": "陕西省",
		"name": "延安市",
		"shortname": "延安"
	},
	"525": {
		"id": "525",
		"pid": "54",
		"pname": "陕西省",
		"name": "汉中市",
		"shortname": "汉中"
	},
	"526": {
		"id": "526",
		"pid": "54",
		"pname": "陕西省",
		"name": "榆林市",
		"shortname": "榆林"
	},
	"527": {
		"id": "527",
		"pid": "54",
		"pname": "陕西省",
		"name": "安康市",
		"shortname": "安康"
	},
	"528": {
		"id": "528",
		"pid": "54",
		"pname": "陕西省",
		"name": "商洛市",
		"shortname": "商洛"
	},
	"529": {
		"id": "529",
		"pid": "56",
		"pname": "江西省",
		"name": "南昌市",
		"shortname": "南昌"
	},
	"530": {
		"id": "530",
		"pid": "56",
		"pname": "江西省",
		"name": "景德镇市",
		"shortname": "景德镇"
	},
	"531": {
		"id": "531",
		"pid": "56",
		"pname": "江西省",
		"name": "萍乡市",
		"shortname": "萍乡"
	},
	"532": {
		"id": "532",
		"pid": "56",
		"pname": "江西省",
		"name": "九江市",
		"shortname": "九江"
	},
	"533": {
		"id": "533",
		"pid": "56",
		"pname": "江西省",
		"name": "新余市",
		"shortname": "新余"
	},
	"534": {
		"id": "534",
		"pid": "56",
		"pname": "江西省",
		"name": "鹰潭市",
		"shortname": "鹰潭"
	},
	"535": {
		"id": "535",
		"pid": "56",
		"pname": "江西省",
		"name": "赣州市",
		"shortname": "赣州"
	},
	"536": {
		"id": "536",
		"pid": "56",
		"pname": "江西省",
		"name": "吉安市",
		"shortname": "吉安"
	},
	"537": {
		"id": "537",
		"pid": "56",
		"pname": "江西省",
		"name": "宜春市",
		"shortname": "宜春"
	},
	"538": {
		"id": "538",
		"pid": "56",
		"pname": "江西省",
		"name": "抚州市",
		"shortname": "抚州"
	},
	"539": {
		"id": "539",
		"pid": "56",
		"pname": "江西省",
		"name": "上饶市",
		"shortname": "上饶"
	},
	"540": {
		"id": "540",
		"pid": "75",
		"pname": "广西",
		"name": "南宁市",
		"shortname": "南宁"
	},
	"541": {
		"id": "541",
		"pid": "75",
		"pname": "广西",
		"name": "柳州市",
		"shortname": "柳州"
	},
	"542": {
		"id": "542",
		"pid": "75",
		"pname": "广西",
		"name": "桂林市",
		"shortname": "桂林"
	},
	"543": {
		"id": "543",
		"pid": "75",
		"pname": "广西",
		"name": "梧州市",
		"shortname": "梧州"
	},
	"544": {
		"id": "544",
		"pid": "75",
		"pname": "广西",
		"name": "北海市",
		"shortname": "北海"
	},
	"545": {
		"id": "545",
		"pid": "75",
		"pname": "广西",
		"name": "防城港市",
		"shortname": "防城港"
	},
	"546": {
		"id": "546",
		"pid": "75",
		"pname": "广西",
		"name": "钦州市",
		"shortname": "钦州"
	},
	"547": {
		"id": "547",
		"pid": "75",
		"pname": "广西",
		"name": "贵港市",
		"shortname": "贵港"
	},
	"548": {
		"id": "548",
		"pid": "75",
		"pname": "广西",
		"name": "玉林市",
		"shortname": "玉林"
	},
	"549": {
		"id": "549",
		"pid": "75",
		"pname": "广西",
		"name": "百色市",
		"shortname": "百色"
	},
	"550": {
		"id": "550",
		"pid": "75",
		"pname": "广西",
		"name": "贺州市",
		"shortname": "贺州"
	},
	"551": {
		"id": "551",
		"pid": "75",
		"pname": "广西",
		"name": "河池市",
		"shortname": "河池"
	},
	"552": {
		"id": "552",
		"pid": "75",
		"pname": "广西",
		"name": "来宾市",
		"shortname": "来宾"
	},
	"553": {
		"id": "553",
		"pid": "75",
		"pname": "广西",
		"name": "崇左市",
		"shortname": "崇左"
	},
	"554": {
		"id": "554",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "哈尔滨市",
		"shortname": "哈尔滨"
	},
	"555": {
		"id": "555",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "齐齐哈尔市",
		"shortname": "齐齐哈尔"
	},
	"556": {
		"id": "556",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "鸡西市",
		"shortname": "鸡西"
	},
	"557": {
		"id": "557",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "鹤岗市",
		"shortname": "鹤岗"
	},
	"558": {
		"id": "558",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "双鸭山市",
		"shortname": "双鸭山"
	},
	"559": {
		"id": "559",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "大庆市",
		"shortname": "大庆"
	},
	"560": {
		"id": "560",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "伊春市",
		"shortname": "伊春"
	},
	"561": {
		"id": "561",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "佳木斯市",
		"shortname": "佳木斯"
	},
	"562": {
		"id": "562",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "七台河市",
		"shortname": "七台河"
	},
	"563": {
		"id": "563",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "牡丹江市",
		"shortname": "牡丹江"
	},
	"564": {
		"id": "564",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "黑河市",
		"shortname": "黑河"
	},
	"565": {
		"id": "565",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "绥化市",
		"shortname": "绥化"
	},
	"566": {
		"id": "566",
		"pid": "78",
		"pname": "黑龙江省",
		"name": "大兴安岭地区",
		"shortname": "大兴安岭"
	},
	"567": {
		"id": "567",
		"pid": "87",
		"pname": "云南省",
		"name": "昆明市",
		"shortname": "昆明"
	},
	"568": {
		"id": "568",
		"pid": "87",
		"pname": "云南省",
		"name": "曲靖市",
		"shortname": "曲靖"
	},
	"569": {
		"id": "569",
		"pid": "87",
		"pname": "云南省",
		"name": "玉溪市",
		"shortname": "玉溪"
	},
	"570": {
		"id": "570",
		"pid": "87",
		"pname": "云南省",
		"name": "保山市",
		"shortname": "保山"
	},
	"571": {
		"id": "571",
		"pid": "87",
		"pname": "云南省",
		"name": "昭通市",
		"shortname": "昭通"
	},
	"572": {
		"id": "572",
		"pid": "87",
		"pname": "云南省",
		"name": "丽江市",
		"shortname": "丽江"
	},
	"573": {
		"id": "573",
		"pid": "87",
		"pname": "云南省",
		"name": "思茅市",
		"shortname": "思茅"
	},
	"574": {
		"id": "574",
		"pid": "87",
		"pname": "云南省",
		"name": "临沧市",
		"shortname": "临沧"
	},
	"575": {
		"id": "575",
		"pid": "87",
		"pname": "云南省",
		"name": "楚雄州",
		"shortname": "楚雄州"
	},
	"576": {
		"id": "576",
		"pid": "87",
		"pname": "云南省",
		"name": "红河州",
		"shortname": "红河州"
	},
	"577": {
		"id": "577",
		"pid": "87",
		"pname": "云南省",
		"name": "文山州",
		"shortname": "文山州"
	},
	"578": {
		"id": "578",
		"pid": "87",
		"pname": "云南省",
		"name": "西双版纳",
		"shortname": "西双版纳"
	},
	"579": {
		"id": "579",
		"pid": "87",
		"pname": "云南省",
		"name": "大理",
		"shortname": "大理"
	},
	"580": {
		"id": "580",
		"pid": "87",
		"pname": "云南省",
		"name": "德宏",
		"shortname": "德宏"
	},
	"581": {
		"id": "581",
		"pid": "87",
		"pname": "云南省",
		"name": "怒江",
		"shortname": "怒江"
	},
	"582": {
		"id": "582",
		"pid": "87",
		"pname": "云南省",
		"name": "迪庆",
		"shortname": "迪庆"
	},
	"583": {
		"id": "583",
		"pid": "100",
		"pname": "山西省",
		"name": "太原市",
		"shortname": "太原"
	},
	"584": {
		"id": "584",
		"pid": "100",
		"pname": "山西省",
		"name": "大同市",
		"shortname": "大同"
	},
	"585": {
		"id": "585",
		"pid": "100",
		"pname": "山西省",
		"name": "阳泉市",
		"shortname": "阳泉"
	},
	"586": {
		"id": "586",
		"pid": "100",
		"pname": "山西省",
		"name": "长治市",
		"shortname": "长治"
	},
	"587": {
		"id": "587",
		"pid": "100",
		"pname": "山西省",
		"name": "晋城市",
		"shortname": "晋城"
	},
	"588": {
		"id": "588",
		"pid": "100",
		"pname": "山西省",
		"name": "朔州市",
		"shortname": "朔州"
	},
	"589": {
		"id": "589",
		"pid": "100",
		"pname": "山西省",
		"name": "晋中市",
		"shortname": "晋中"
	},
	"590": {
		"id": "590",
		"pid": "100",
		"pname": "山西省",
		"name": "运城市",
		"shortname": "运城"
	},
	"591": {
		"id": "591",
		"pid": "100",
		"pname": "山西省",
		"name": "忻州市",
		"shortname": "忻州"
	},
	"592": {
		"id": "592",
		"pid": "100",
		"pname": "山西省",
		"name": "临汾市",
		"shortname": "临汾"
	},
	"593": {
		"id": "593",
		"pid": "100",
		"pname": "山西省",
		"name": "吕梁市",
		"shortname": "吕梁"
	},
	"594": {
		"id": "594",
		"pid": "105",
		"pname": "贵州省",
		"name": "贵阳市",
		"shortname": "贵阳"
	},
	"595": {
		"id": "595",
		"pid": "105",
		"pname": "贵州省",
		"name": "六盘水市",
		"shortname": "六盘水"
	},
	"596": {
		"id": "596",
		"pid": "105",
		"pname": "贵州省",
		"name": "遵义市",
		"shortname": "遵义"
	},
	"597": {
		"id": "597",
		"pid": "105",
		"pname": "贵州省",
		"name": "安顺市",
		"shortname": "安顺"
	},
	"598": {
		"id": "598",
		"pid": "105",
		"pname": "贵州省",
		"name": "铜仁地区",
		"shortname": "铜仁"
	},
	"599": {
		"id": "599",
		"pid": "105",
		"pname": "贵州省",
		"name": "黔西南州",
		"shortname": "黔西南州"
	},
	"600": {
		"id": "600",
		"pid": "105",
		"pname": "贵州省",
		"name": "毕节地区",
		"shortname": "毕节"
	},
	"601": {
		"id": "601",
		"pid": "105",
		"pname": "贵州省",
		"name": "黔东南州",
		"shortname": "黔东南州"
	},
	"602": {
		"id": "602",
		"pid": "105",
		"pname": "贵州省",
		"name": "黔南州",
		"shortname": "黔南州"
	},
	"603": {
		"id": "603",
		"pid": "108",
		"pname": "宁夏",
		"name": "银川市",
		"shortname": "银川"
	},
	"604": {
		"id": "604",
		"pid": "108",
		"pname": "宁夏",
		"name": "石嘴山市",
		"shortname": "石嘴山"
	},
	"605": {
		"id": "605",
		"pid": "108",
		"pname": "宁夏",
		"name": "吴忠市",
		"shortname": "吴忠"
	},
	"606": {
		"id": "606",
		"pid": "108",
		"pname": "宁夏",
		"name": "固原市",
		"shortname": "固原"
	},
	"607": {
		"id": "607",
		"pid": "108",
		"pname": "宁夏",
		"name": "中卫市",
		"shortname": "中卫"
	},
	"608": {
		"id": "608",
		"pid": "126",
		"pname": "内蒙古",
		"name": "呼和浩特市",
		"shortname": "呼和浩特"
	},
	"609": {
		"id": "609",
		"pid": "126",
		"pname": "内蒙古",
		"name": "包头市",
		"shortname": "包头"
	},
	"610": {
		"id": "610",
		"pid": "126",
		"pname": "内蒙古",
		"name": "乌海市",
		"shortname": "乌海"
	},
	"611": {
		"id": "611",
		"pid": "126",
		"pname": "内蒙古",
		"name": "赤峰市",
		"shortname": "赤峰"
	},
	"612": {
		"id": "612",
		"pid": "126",
		"pname": "内蒙古",
		"name": "通辽市",
		"shortname": "通辽"
	},
	"613": {
		"id": "613",
		"pid": "126",
		"pname": "内蒙古",
		"name": "鄂尔多斯市",
		"shortname": "鄂尔多斯"
	},
	"614": {
		"id": "614",
		"pid": "126",
		"pname": "内蒙古",
		"name": "呼伦贝尔市",
		"shortname": "呼伦贝尔"
	},
	"615": {
		"id": "615",
		"pid": "126",
		"pname": "内蒙古",
		"name": "巴彦淖尔市",
		"shortname": "巴彦淖尔"
	},
	"616": {
		"id": "616",
		"pid": "126",
		"pname": "内蒙古",
		"name": "乌兰察布市",
		"shortname": "乌兰察布"
	},
	"617": {
		"id": "617",
		"pid": "126",
		"pname": "内蒙古",
		"name": "兴安盟",
		"shortname": "兴安盟"
	},
	"618": {
		"id": "618",
		"pid": "126",
		"pname": "内蒙古",
		"name": "锡林郭勒盟",
		"shortname": "锡林郭勒盟"
	},
	"619": {
		"id": "619",
		"pid": "126",
		"pname": "内蒙古",
		"name": "阿拉善盟",
		"shortname": "阿拉善盟"
	},
	"620": {
		"id": "620",
		"pid": "197",
		"pname": "海南省",
		"name": "海口市",
		"shortname": "海口"
	},
	"621": {
		"id": "621",
		"pid": "197",
		"pname": "海南省",
		"name": "三亚市",
		"shortname": "三亚"
	},
	"622": {
		"id": "622",
		"pid": "197",
		"pname": "海南省",
		"name": "五指山市",
		"shortname": "五指山"
	},
	"623": {
		"id": "623",
		"pid": "197",
		"pname": "海南省",
		"name": "琼海市",
		"shortname": "琼海"
	},
	"624": {
		"id": "624",
		"pid": "197",
		"pname": "海南省",
		"name": "儋州市",
		"shortname": "儋州"
	},
	"625": {
		"id": "625",
		"pid": "197",
		"pname": "海南省",
		"name": "文昌市",
		"shortname": "文昌"
	},
	"626": {
		"id": "626",
		"pid": "197",
		"pname": "海南省",
		"name": "万宁市",
		"shortname": "万宁"
	},
	"627": {
		"id": "627",
		"pid": "197",
		"pname": "海南省",
		"name": "东方市",
		"shortname": "东方"
	},
	"628": {
		"id": "628",
		"pid": "197",
		"pname": "海南省",
		"name": "定安县",
		"shortname": "定安"
	},
	"629": {
		"id": "629",
		"pid": "197",
		"pname": "海南省",
		"name": "屯昌县",
		"shortname": "屯昌"
	},
	"630": {
		"id": "630",
		"pid": "197",
		"pname": "海南省",
		"name": "澄迈县",
		"shortname": "澄迈"
	},
	"631": {
		"id": "631",
		"pid": "197",
		"pname": "海南省",
		"name": "临高县",
		"shortname": "临高"
	},
	"632": {
		"id": "632",
		"pid": "197",
		"pname": "海南省",
		"name": "白沙黎族自治县",
		"shortname": "白沙"
	},
	"633": {
		"id": "633",
		"pid": "197",
		"pname": "海南省",
		"name": "昌江黎族自治县",
		"shortname": "昌江"
	},
	"634": {
		"id": "634",
		"pid": "197",
		"pname": "海南省",
		"name": "乐东黎族自治县",
		"shortname": "乐东"
	},
	"635": {
		"id": "635",
		"pid": "197",
		"pname": "海南省",
		"name": "陵水黎族自治县",
		"shortname": "陵水"
	},
	"636": {
		"id": "636",
		"pid": "197",
		"pname": "海南省",
		"name": "保亭黎族苗族自治县",
		"shortname": "保亭"
	},
	"637": {
		"id": "637",
		"pid": "197",
		"pname": "海南省",
		"name": "琼中黎族苗族自治县",
		"shortname": "琼中"
	},
	"638": {
		"id": "638",
		"pid": "197",
		"pname": "海南省",
		"name": "西沙群岛",
		"shortname": "西沙群岛"
	},
	"639": {
		"id": "639",
		"pid": "197",
		"pname": "海南省",
		"name": "南沙群岛",
		"shortname": "南沙群岛"
	},
	"640": {
		"id": "640",
		"pid": "197",
		"pname": "海南省",
		"name": "中沙群岛的岛礁及其海域",
		"shortname": "中沙群岛的岛礁及其海域"
	},
	"641": {
		"id": "641",
		"pid": "202",
		"pname": "青海省",
		"name": "西宁市",
		"shortname": "西宁"
	},
	"642": {
		"id": "642",
		"pid": "202",
		"pname": "青海省",
		"name": "海东地区",
		"shortname": "海东"
	},
	"643": {
		"id": "643",
		"pid": "202",
		"pname": "青海省",
		"name": "海州",
		"shortname": "海州"
	},
	"644": {
		"id": "644",
		"pid": "202",
		"pname": "青海省",
		"name": "黄南州",
		"shortname": "黄南州"
	},
	"645": {
		"id": "645",
		"pid": "202",
		"pname": "青海省",
		"name": "海南州",
		"shortname": "海南州"
	},
	"646": {
		"id": "646",
		"pid": "202",
		"pname": "青海省",
		"name": "果洛州",
		"shortname": "果洛州"
	},
	"647": {
		"id": "647",
		"pid": "202",
		"pname": "青海省",
		"name": "玉树州",
		"shortname": "玉树州"
	},
	"648": {
		"id": "648",
		"pid": "202",
		"pname": "青海省",
		"name": "海西州",
		"shortname": "海西州"
	},
	"649": {
		"id": "649",
		"pid": "7",
		"pname": "北京市",
		"name": "北京市",
		"shortname": "北京"
	},
	"650": {
		"id": "650",
		"pid": "34",
		"pname": "重庆市",
		"name": "重庆市",
		"shortname": "重庆"
	},
	"651": {
		"id": "651",
		"pid": "21",
		"pname": "上海市",
		"name": "上海市",
		"shortname": "上海"
	},
	"652": {
		"id": "652",
		"pid": "42",
		"pname": "天津市",
		"name": "天津市",
		"shortname": "天津"
	}
}