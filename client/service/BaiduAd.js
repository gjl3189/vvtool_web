/**
 * 百度推广广告 信息
 * @constructor
 */
var AdInfo = function(){
    this.desc = "";
    this.title = "";
    this.url = "";
    this.domain = "";
    this.posseq = 0
    this.seq = 0;
    this.layout = 0;
}

var AdLayout = function(){
    this.AD_LAYOUT_TOP = {"num":1,"lang":"顶"}
    this.AD_LAYOUT_LEFT = {"num":2,"lang":"左"}
    this.AD_LAYOUT_RIGHT = {"num":3,"lang":"右"}
    this.AD_LAYOUT_HEALTH = {"num":4,"lang":"健康"}
}

exports.AdInfo = AdInfo;
exports.AdLayout = AdLayout;