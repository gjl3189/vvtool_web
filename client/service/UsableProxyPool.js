module.exports = new UsableProxyPool();

function UsableProxyPool(){
    this.hashlist = {};
}

UsableProxyPool.prototype.add = function(proxy){
    var k = proxy.host+":"+proxy.port;
    this.hashlist[k] = proxy;
}

UsableProxyPool.prototype.del = function(proxy){
    var k = proxy.host+":"+proxy.port;
    delete this.hashlist[k];
}

UsableProxyPool.prototype.exists = function(proxy){
    var k = proxy.host+":"+proxy.port;
    if(this.hashlist[k]!=undefined){
        return true;
    }else{
        return false;
    }
}

UsableProxyPool.prototype.all = function(){
    return this.hashlist;
}