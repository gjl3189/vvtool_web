/**
 * 代理ip池，还没验证可用
 * @type {ProxyPool}
 */
module.exports = new ProxyPool();

function ProxyPool(){
    //单例模式
    if(typeof arguments.callee.instance === "object"){
        return arguments.callee.instance;
    }
    this.list = [];
    this.hlist = {};
    arguments.callee.instance = this;
}

ProxyPool.prototype.push = function(proxy){
    if(proxy.host==undefined || proxy.port==undefined){
        return false;
    }
    var k = proxy.host+":"+proxy.port;
    if(this.hlist[k] != undefined){
        return true;
    }
    this.hlist[k] = true;
    return this.list.push(proxy);
}

ProxyPool.prototype.pop = function(){
    if(this.list.length>0){
        var proxy =  this.list.pop();
        var lkey = this.lkey(proxy);
        delete this.hlist[lkey];
        return proxy;
    }
}

ProxyPool.prototype.unshift = function(proxy){
    var k = this.lkey(proxy);
    if(this.hlist[k] != undefined){
        return true;
    }
    this.hlist[k] = true;

    return this.list.unshift(proxy);
}

ProxyPool.prototype.shift = function(){
    if(this.list.length>0){
        var proxy = this.list.shift();
        var lkey = this.lkey(proxy);
        delete this.hlist[lkey];
        return proxy;
    }
}

ProxyPool.prototype.len = function(){
    return this.list.length;
}

ProxyPool.prototype.lkey = function(proxy){
    if(proxy.host==undefined || proxy.port==undefined){
        return "";
    }
    return proxy.host+":"+proxy.port;
}

