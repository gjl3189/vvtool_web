/**
 * 解析百度推广广告
 */

var $ = require("jQuery");
var BaiduAd = require("./BaiduAd.js");

module.exports = new AnalysisBaiduAd();

function AnalysisBaiduAd(){

}

AnalysisBaiduAd.prototype.analysis = function(html){
    var htmlDom = $(html);
    var leftcount = 0;
    var adlist = new Array();
    var len = 0;

    var layoutList = new Array();
    //顶部广告
    var topadlist = new Array();
    var adlayoutSt = new BaiduAd.AdLayout();

    $(htmlDom).find("#content_left .EC_ppim_top").each(function(i){
        var ad = new BaiduAd.AdInfo();
        ad.url = $(this).find(".EC_url").text();
        ad.title = $(this).find(".EC_title").text();
        ad.desc = $(this).find(".EC_desc").text();
        ad.posseq = i + 1;
        ad.pos = adlayoutSt.AD_LAYOUT_TOP;
        ad.seq = i + 1;
        leftcount = ad.seq;
        //ad.domain = $.url.setUrl(ad.url).attr("host")
        topadlist.push(ad);
    });
    if(topadlist.length>0){
        for(var i=0;i<topadlist.length/2;i++){
            adlist.push(topadlist[i]);
        }
    }

    if(topadlist.length>0){
        layoutList.push({"layout":adlayoutSt.AD_LAYOUT_TOP,"len":topadlist.length/2})
    }

    len = 0;
    //百度健康
    $(htmlDom).find("#content_left .ecl-health-pagelist-item").each(function(i){
        var ad = new BaiduAd.AdInfo();
        ad.title = $(this).find(".pagelist-name").text();
        ad.desc = $(this).find(".pagelist-creative").text();
        ad.url = ad.title.substr(ad.title.indexOf("】")+1);
        ad.posseq = i + 1;
        ad.pos = adlayoutSt.AD_LAYOUT_HEALTH;
        ad.seq = leftcount + i + 1;
        ad.domain = ad.url;
        leftcount = ad.seq;
        adlist.push(ad);
        len = i+1;

    });

    if(len>0){
        layoutList.push({"layout":adlayoutSt.AD_LAYOUT_HEALTH,"len":len})
    }

    //左边广告
    len = 0;
    $(htmlDom).find("#content_left .ec_pp_top").each(function(i){
        var ad = new BaiduAd.AdInfo();
        var adlayoutSt = new BaiduAd.AdLayout();

        ad.url = $(this).find(".ec_url").text();
        ad.title = $(this).find(".ec_title").text();
        ad.desc = $(this).find(".ec_desc").text();
        ad.posseq = i + 1;
        ad.pos = adlayoutSt.AD_LAYOUT_LEFT;
        ad.seq = leftcount + i + 1;
        leftcount = ad.seq;

        //ad.domain = $.url.setUrl(ad.url).attr("host")
        adlist.push(ad);
        len = i+1;
    });
    if(len>0){
        layoutList.push({"layout":adlayoutSt.AD_LAYOUT_LEFT,"len":len})
    }

    //右边广告
    len = 0;
    $(htmlDom).find("#ec_im_container .EC_im").each(function(i){
        var ad = new BaiduAd.AdInfo();
        ad.url = $(this).find(".EC_url").text();
        ad.title = $(this).find(".EC_t").text();
        ad.desc = $(this).find(".EC_desc").text();
        ad.posseq = i + 1;
        ad.pos = adlayoutSt.AD_LAYOUT_RIGHT;
        ad.seq = leftcount + i + 1;
        ad.layout = adlayoutSt.AD_LAYOUT_LEFT;;

        //ad.domain = $.url.setUrl(ad.url).attr("host")
        adlist.push(ad);
        len = i+1;
    });
    if(len>0){
        layoutList.push({"layout":adlayoutSt.AD_LAYOUT_RIGHT,"len":len})
    }
    return { ad:adlist,layout:layoutList};
}


